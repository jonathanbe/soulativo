var elixir = require('laravel-elixir'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    rjs = require('gulp-requirejs'),
    notify = require('gulp-notify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    /*
     * Admin tasks
     */
    elixir.config.assetsPath = 'resources/assets/backend';
    elixir.config.publicPath = 'public/backend';

    mix.sass('app.scss');

    new elixir.Task('requirejs', function () {
        return rjs({
            baseUrl: 'resources/assets/backend/js',
            name: 'requireLib',
            out: 'app.js',
            include: [
                'app',
                'async',
                'bootstrap',
                'jquery',
                'underscore',
                'picker',
                'dante',
                'moment',
                'clndr',
                'query-builder',
                'sortable',
                'awesomplete'
            ],
            paths: {
                'async': '../../../../node_modules/requirejs-plugins/src/async',
                'bootstrap': '../../../../node_modules/bootstrap/dist/js/bootstrap',
                'jquery': '../../../../node_modules/jquery/dist/jquery',
                'jquery-validate': '../../../../node_modules/jquery-validation/dist/jquery.validate',
                'underscore': '../../../../node_modules/underscore/underscore',
                'picker': '../../../../node_modules/pickadate/lib/picker',
                'picker/date': '../../../../node_modules/pickadate/lib/picker.date',
                'picker/time': '../../../../node_modules/pickadate/lib/picker.time',
                'moment': '../../../../node_modules/moment/min/moment-with-locales',
                'clndr': '../../../../node_modules/clndr/src/clndr',
                'requireLib': '../../../../node_modules/requirejs/require',
                'awesomplete': '../../../../node_modules/awesomplete/awesomplete',
                'sortable': '../../../../node_modules/sortablejs/Sortable',
                'sanitize': '../../vendor/js/sanitize',
                'dante': '../../vendor/js/dante-editor',
                'query-builder': '../../vendor/js/query-builder.standalone'
            },
            packages: [
                'address',
                'customer',
                'language',
                'password',
                'user',
                'product',
                'package',
                'category',
                'list',
                'page',
                'form',
                'post',
                'media',
                'promotion',
                'booking',
                'order',
                'store',
                'coupon',
                'blockout',
                'views'
            ],
            shim: {
                app: [
                    'views',
                    'address',
                    'customer',
                    'language',
                    'password',
                    'user',
                    'package',
                    'category',
                    'list',
                    'media',
                    'promotion',
                    'page',
                    'form',
                    'booking',
                    'order',
                    'store',
                    'coupon',
                    'blockout',
                    'post'
                ],
                mushi: [
                  'jquery',
                  'jquery-validate'
                ],
                dante: [
                  'jquery',
                  'underscore',
                  'sanitize'
                ],
                bootstrap: [
                  'jquery'
                ],
                underscore: {
                    exports: '_'
                },
                clndr: [
                    'underscore',
                    'moment'
                ],
                jquery: {
                    exports: '$'
                },
                sortable: {
                    exports: 'Sortable'
                }
            }
        })
        .pipe(gulp.dest('public/backend/js'))
        .pipe(notify("Generated file: <%= file.relative %>"));
    })
    .watch('resources/assets/backend/js/**/*.js');
});
