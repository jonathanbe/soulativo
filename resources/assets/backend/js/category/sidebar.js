define([], function () {
    var CategorySidebar = function (options) {
        this.init(options);
    };

    CategorySidebar.defaults = {
        element : '',
        inputName : 'product[categories][]'
    };

    CategorySidebar.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, CategorySidebar.defaults, options);
            this.initFooter();
            this.onSubmit();
            this.checkParent($('input[type="checkbox"]', this.options.element));
        },

        checkParent : function (elements) {
            var that = this;

            $(elements).on('change', function () {
                if($(this).data('parent') != 0){
                    var parent = $('input[type=checkbox][value=' + $(this).data('parent') + ']', that.options.element);

                    if($(this).prop('checked')){
                        $(parent).prop('checked', true);
                    } else {
                        $(parent).prop('checked', false);
                    }
                }
            });
        },

        initFooter : function () {
            var target = $(this.options.element);

            $('footer a', target).on('click', function(){
                $('> div', this.parentNode).removeClass('hidden-xs-up p-t-1');
                $(this).addClass('hidden-xs-up');
                return false;
            });
        },

        onSubmit : function () {
            var that = this,
                target = $(this.options.element),
                url = '//' + location.host + target.data('action'),
                input = $('footer input[type="text"]', target),
                button = $('footer button', target),
                table = $('table', target),
                parent = $('footer select' ,target);

            button.on('click', function() {
                $.ajax({
                    url : url,
                    type : 'POST',
                    data : {
                        '_token' : $('input[name="_token"]').val(),
                        'category[name]' : input.val(),
                        'category[parent_id]' : parent.val()
                    },
                    dataType : 'json',
                    success : function(result){
                        var parent = $('#category-' + result.input.parent_id),
                            data = '<tr>'+
                                        '<td>'+
                                            '<label class="m-l-' + ((result.input.depth != null) ? result.input.depth - 1 : 0) + '">'+
                                                '<input type="checkbox" name="' + that.options.inputName + '" class="m-l-1 m-r-1" data-parent="' + ((result.input.depth == 1) ? 0 : result.input.parent_id) + '" value="' + result.input.id + '" checked> '+ result.input.name +
                                            '</label>'+
                                        '</td>'+
                                    '</tr>';

                        if(parent.length > 0){
                            parent = parent.closest('tr').after(data);
                        }else{
                            parent = $('tbody', table).prepend(data);
                        }

                        that.checkParent(parent);
                    },
                    complete : function(result){

                    }
                });

                return false;
            });
        }
    };

    return CategorySidebar;
});
