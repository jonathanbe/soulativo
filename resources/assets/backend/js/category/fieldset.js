define([
    'language',
    'page/slug'
], function (Lang, Slug) {

    var CategoryFieldset= function (options) {
        this.init(options);
    };

    CategoryFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        }
    };

    CategoryFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, CategoryFieldset.defaults, options);
            this.slug = new Slug({
                title : 'input[name="category[name]"]',
                slug : 'input[name="category[slug]"]'
            });
        }
    };

    return CategoryFieldset;
});
