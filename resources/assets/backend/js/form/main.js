define([
	'jquery-validate'
], function () {
	/* Private Variables */
	var Form = function (element, options) {
			this.element = $(element);
			this.init(options);
		};

	/* Private Methods */
	Form.defaults = {
		validate: {}
	};

	Form.prototype = {
		/* Public Variables */
		element : null,
		options : null,

		/* Public Methods */
		init : function(options){
			this.options = $.extend(true, {}, Form.defaults, options);
			this.element.validate(this.options.validate);
		}
	};

	return Form;
});
