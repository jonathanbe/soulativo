define([
    'language'
], function (Lang) {

    var UserFieldset= function (options) {
        this.init(options);
    };

    UserFieldset.defaults = {
        'validate' : {
            rules : {
                'user[first_name]' : 'required',
                'user[last_name]' : 'required',
                'user[email]' : {
                    'required' : true,
                    'email' : true
                }
            },
            messages : {
                'user[first_name]' : Lang.validate.user.first_name,
                'user[last_name]' : Lang.validate.user.last_name,
                'user[email]' : Lang.validate.user.email
            }
        }
    };

    UserFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, UserFieldset.defaults, options);
        }
    };

    return UserFieldset;
});
