define([

], function () {
    var Compare = function (options) {
        this.init(options);
    };

    Compare.defaults = {
        field : 'input[name="user[password]"]',
        confirm : 'input[name="user[password_confirmation]"]',
        successClass : 'success',
        errorClass : 'danger'
    };

    /* Public Methods */
    Compare.prototype = {
        options : {},
        init : function (options) {
            that = this;
            this.options = $.extend(true, {}, Compare.defaults, options);

            $(this.options.field).on('keyup', function () { return that.passwordsCompare(that); } );
            $(this.options.confirm).on('keyup', function () { return that.passwordsCompare(that); } );
        },

        passwordsCompare : function (that) {
            if($(that.options.field).val() === $(that.options.confirm).val() &&
                $(that.options.field).val() != '')
                return that.passwordsMatch();
            else
                return that.passwordsFail();
        },

        passwordsMatch : function () {
            $(that.options.field).parent().addClass('has-' + that.options.successClass).removeClass('has-' + that.options.errorClass);
            $(that.options.confirm).parent().addClass('has-' + that.options.successClass).removeClass('has-' + that.options.errorClass);
            $(that.options.field).addClass('form-control-' + that.options.successClass).removeClass('form-control-' + that.options.errorClass);
            $(that.options.confirm).addClass('form-control-' + that.options.successClass).removeClass('form-control-' + that.options.errorClass);
            return true;
        },

        passwordsFail : function () {
            $(that.options.field).parent().addClass('has-' + that.options.errorClass).removeClass('has-' + that.options.successClass);
            $(that.options.confirm).parent().addClass('has-' + that.options.errorClass).removeClass('has-' + that.options.successClass);
            $(that.options.field).addClass('form-control-' + that.options.errorClass).removeClass('form-control-' + that.options.successClass);
            $(that.options.confirm).addClass('form-control-' + that.options.errorClass).removeClass('form-control-' + that.options.successClass);
            return false;
        }
    };

    return Compare;
});
