define([
    'promotion/fieldset',
    'form'
], function (Promotion, Form) {
    return {
        init : function () {
            var that = this;

            this.promotion = new Promotion('input[name="promotion[rules]"]');
            this.validation = $.extend(true, {},
                this.promotion.options.validate,
                {
                    submitHandler : function () {
                        var builder = $(that.promotion.options.builder),
                            query = builder.queryBuilder('getRules');

                        if(!builder.queryBuilder('validate')){
                            return false;
                        }else{
                            that.promotion.element.val(JSON.stringify(query, null, 2));
                            return true;
                        }
                    }
                }
            );
            this.form = new Form('#form-promotion', {
                validate : this.validation,
            });
        }
    }
});
