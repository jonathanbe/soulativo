define([
    'user/fieldset',
    'booking/fieldset',
    'order/aside',
    'order/modal',
    'form'
], function (User, Booking, OrderAside, OrderModal, Form) {
    return {
        init : function () {
            this.booking = new Booking();
            this.aside = new OrderAside();
            this.validation = $.extend(true, {},
                this.booking.options.validate,
                this.aside.options.validate,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );
            this.form = new Form('#form-booking', {
                validate : this.validation
            });

            this.modal = new OrderModal();
        }
    }
});
