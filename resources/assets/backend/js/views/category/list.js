define([
    'list/bin',
    'list/paginate',
    'list/search'
], function (Bin, Paginate, Search) {
    return {
        init : function () {
            this.search = new Search({
                form : 'form#search',
                redirect : 'admin/category/edit/'
            });

            this.allBin = new Bin({
                form : 'form#all'
            });

            this.trashBin = new Bin({
                form : 'form#trash'
            });

            this.paginate = new Paginate({
                button : '#all a.paginate',
                target : '#all table tbody',
                template : '#category-list-template'
            });
        }
    }
});
