define([
    'category/fieldset',
    'media/gallery',
    'form'
], function (Category, Gallery, Form) {
    return {
        init : function () {
            this.category = new Category();
            this.validation = $.extend(true, {},
                this.category.options.validate,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );
            this.form = new Form('#form-category', {
                validate : this.validation,
            });
            this.thumbnail = new Gallery('#thumbnail-set', { target : '#thumbnail-wrapper' });
        }
    }
});
