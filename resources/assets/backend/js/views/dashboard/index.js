define([

], function () {
    return {
        current : null,

        init : function () {
            var that = this;

            $('.featured .card header button').on('click', function () {
                that.current = $(this).closest('.card');
            });

            $('.replace .card header button').on('click', function () {
                var button = $(this),
                    id = button.closest('.card').data('id'),
                    position = that.current.data('position');
                    url = '/admin/dashboard/package/'+ id +'/featured/' + position;

                $.ajax({
                    url : url,
                    type : 'GET',
                    dataType : 'json',
                    success : function(result){
                        if(!result.error)
                            that.replace(result);
                    },
                    complete : function(){
                        button.closest('.modal').modal('hide');
                    }
                });
            })
        },

        replace : function (data) {
            var that = this,
                template = _.template($('#package-loop-template').html());
            this.current.html(template(data.package));

            $('header button', this.current).on('click', function () {
                that.current = $(this).closest('.card');
            });
        }
    }
});
