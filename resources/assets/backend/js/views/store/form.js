define([
    'store/fieldset',
    'address/fieldset',
    'form'
], function (Store, Address, Form) {
    return {
        init : function () {
            var that = this;

            this.store = new Store();
            this.address = new Address();
            this.validation = $.extend(true, {},
                this.store.options.validate,
                this.address.options.validate
            );
            this.form = new Form('#form-store', {
                validate : this.validation,
            });
        }
    }
});
