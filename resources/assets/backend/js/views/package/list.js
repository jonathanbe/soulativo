define([
    'list/bin',
    'list/featured',
    'list/paginate',
    'list/search'
], function (Bin, Featured, Paginate, Search) {
    return {
        init : function () {
            var that = this;

            this.search = new Search({
                form : 'form#search',
                redirect : 'admin/package/edit/'
            });

            this.allBin = new Bin({
                form : 'form#all'
            });

            this.trashBin = new Bin({
                form : 'form#trash'
            });

            this.featured = new Featured({
                action : 'package/featured/set'
            });

            this.paginate = new Paginate({
                button : '#all a.paginate',
                target : '#all table tbody',
                template : '#package-list-template',
                callback : function() { that.featured.reset() }
            });
        }
    }
});
