define([
    'blockout/fieldset',
    'form'
], function (Blockout, Form) {
    return {
        init : function () {
            this.blockout = new Blockout();
            this.validation = $.extend(true, {},
                this.blockout.options.validate
            );
            this.form = new Form('#form-blockout', {
                validate : this.validation
            });
        }
    }
});
