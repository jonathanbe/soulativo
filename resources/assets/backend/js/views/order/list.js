define([
    'list/paginate',
    'list/search'
], function (Paginate, Search) {
    return {
        init : function () {
            this.search = new Search({
                form : 'form#search',
                redirect : 'admin/order/edit/'
            });

            this.paginate = new Paginate({
                button : 'a.paginate',
                target : 'table tbody',
                template : '#order-list-template'
            });
        }
    }
});
