define([
    'language',
    'page/slug',
    'dante'
], function (Lang, Slug) {

    var PostFieldset= function (options) {
        this.init(options);
    };

    PostFieldset.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        },
        dante : {
            el: "#page-editor",
            upload_url: "/media/upload/editor",
            store_url: ""
        }
    };

    PostFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, PostFieldset.defaults, options);
            this.slug = new Slug({
                title : 'input[name="post[title]"]',
                slug : 'input[name="post[slug]"]'
            });
            this.initEditor();
        },

        initEditor : function () {
            this.content = new Dante.Editor(this.options.dante);
            this.content.start();
        }
    };

    return PostFieldset;
});
