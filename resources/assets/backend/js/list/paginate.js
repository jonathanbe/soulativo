define([

], function () {

    var ListPaginate= function (options) {
        this.init(options);
    };

    ListPaginate.defaults = {
        button : '',
        target : '',
        template : '',
        callback : function() {}
    };

    ListPaginate.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, ListPaginate.defaults, options);

            $(this.options.button).on('click', function () {
                that.next();
                return false;
            });
        },

        load : function (page) {
            var that = this,
                url = $(this.options.button).prop('href') + '/paginate/' + page + '/' + $(this.options.button).data('limit'),
                data = {};

            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    if(result.length > 0){
                        var template = _.template($(that.options.template).html());

                        // Appends the result to the list
                        $(that.options.target).append(template({result : result}));

                        // Updates page index
                        $(that.options.button).data('page', page);

                        // Returns the result
                        data = result;

                        that.options.callback();
                    }else{
                        data = null;
                    }
                },
                complete : function(result){
                    console.log(result);
                }
            });

            return data;
        },

        next : function () {
            var current = $(this.options.button).data('page');

            // Return null if page do not exists
            return this.load(current + 1);
        }
    };

    return ListPaginate;
});
