define([
    'language',
], function (Lang) {
    var OrderModal = function (options) {
        this.init(options);
    };

    OrderModal.defaults = {
        'validate' : {
            rules : {

            },
            messages : {

            }
        }
    };

    /* Public Methods */
    OrderModal.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, OrderModal.defaults, options);

            // Add new coupon button was clicked
            $('#form-order-sendmail button.add, #form-remove-coupon button.add').on('click', function () {
                that.showNewCoupon();
                return false;
            });

            // Remove coupon button was clicked
            $('#form-remove-coupon button.remove').on('click', function () {
                that.removeCoupon();
                return false;
            });

            // Apply coupon form was submited
            $('#form-add-coupon').on('submit', function () {
                that.applyCoupon();
                return false;
            });

            // Cancel button was clicked
            $('#form-add-coupon button.cancel').on('click', function () {
                that.showExistingCoupon();
                return false;
            });

            // Sendmail form was submited
            $('#order-modal button.submit').on('click', function () {
                that.sendMail();
            });
        },

        showNewCoupon : function () {
            $('#form-order-sendmail .empty-coupon').slideUp(function () {
                $('#form-remove-coupon').slideUp(function () {
                    $('#form-add-coupon').slideDown();
                });
            });
        },

        showExistingCoupon : function () {
            $('#form-order-sendmail .empty-coupon').slideUp(function () {
                $('#form-add-coupon').slideUp(function () {
                    $('#form-remove-coupon').slideDown();
                });
            });
        },

        sendMail : function () {
            var url = $('#form-order-sendmail').prop('action'),
                formData = $('#form-order-sendmail').serialize();

            $('#order-modal button.submit').prop('disabled', true);

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                success : function(result){
                    console.log(result);
                    if(result.error == false){
                        $('#order-modal').modal('hide');
                        // FIXME: We need to clear up the form message textarea
                    }
                },
                complete : function(result){
                    $('#order-modal button.submit').prop('disabled', false);
                }
            });
        },

        removeCoupon : function () {
            var url = $('#form-remove-coupon').prop('action'),
                formData = $('#form-remove-coupon').serialize();

            $('#form-remove-coupon button.remove').prop('disabled', true);

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                success : function(result){
                    var old = parseInt($('.price .subtotal .old span').html());

                    $('.price .subtotal .discount span').html("0.0");
                    $('.price .total span').html(old);

                    $('#form-add-coupon').slideUp(function () {
                        $('#form-remove-coupon').slideUp(function () {
                            $('#form-order-sendmail .empty-coupon').slideDown();
                        });
                    });
                },
                complete : function(result){
                    $('#form-remove-coupon button.remove').prop('disabled', false);
                }
            });
        },

        applyCoupon : function () {
            var url = $('#form-add-coupon').prop('action'),
                formData = $('#form-add-coupon').serialize();

            $('#form-add-coupon button.apply').prop('disabled', true);

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                success : function(result){
                    var old = parseInt($('.price .subtotal .old span').html());

                    if(result.coupon){
                        var discount = (old / 100) * parseInt(result.coupon.value);
                        var total = old - discount;

                        $('input[name="coupon[key]"]').val(result.coupon.key);
                        $('input[name="coupon[value]"]').val(result.coupon.value);
                        $('.price .subtotal .discount span').html(discount);
                        $('.price .total span').html(total);
                    }

                    $('#form-order-sendmail .empty-coupon').slideUp(function () {
                        $('#form-add-coupon').slideUp(function () {
                            $('#form-remove-coupon').slideDown();
                        });
                    });
                },
                complete : function(result){
                    $('#form-add-coupon button.apply').prop('disabled', false);
                }
            });
        },

        setTotal : function () {
            // FIXME: We need to get the new order object and then set the fixed price
        }
    };

    return OrderModal;
});
