define([
    'language',
    'moment',
    'clndr'
], function (Lang, Moment) {
    var BlockoutFieldset = function (options) {
        this.init(options);
    };

    BlockoutFieldset.defaults = {
        'calendar' : '#calendar',
        'input' : '#package-blockouts',
        'validate' : {
            rules : {

            },
            messages : {

            }
        }
    };

    /* Public Methods */
    BlockoutFieldset.prototype = {
        init : function (options) {
            var that = this;

            this.options = $.extend(true, {}, BlockoutFieldset.defaults, options);

            $(this.options.calendar).clndr({
				template: $('#blockout-calendar-template').html(),
				daysOfTheWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				numberOfRows: 5,
				clickEvents : {
					click : function (result) {
						if (!$(result.element).hasClass('past')) {
                            // Check if blockout already exists
                            if(!that.removeBlockout(result.date, result.element)){
                                that.addBlockout(result.date, result.element);
                            }

                            // Updates the input field
                            that.updateInput();
						}
					}
				}
			});

            // If the input is already populated, then insert the default blockouts
            if($(this.options.input).val() != ""){
                var data = $.parseJSON($(this.options.input).val());
                $.each(data, function (){
                    $(that.options.calendar).clndr().addEvents([
                        { date : Moment(new Date(this)) }
                    ]);
                });
            }
        },

        addBlockout : function (date, element) {
            var that = this,
                url = '/admin/blockout/create/' + date.format('YYYY-MM-DD') + '/package/' + $(this.options.input).data('id');

            // Submit ajax to add the selected blockout
            $.ajax({
                url : url,
                type : 'GET',
                dataType : 'json',
                success : function (result) {
                    $(that.options.calendar).clndr().addEvents([
                        { date : date }
                    ]);
                },
                complete : function () {
                    // $(element).removeClass('disabled');
                }
            });
        },

        removeBlockout : function (date, element) {
            var url = url = '/admin/blockout/destroy/' + date.format('YYYY-MM-DD') + '/package/' + $(this.options.input).data('id'),
                exists = false;

            $(this.options.calendar).clndr().removeEvents(function (event) {
                if(event.date.format('L') == date.format('L')){
                    exists = true;

                    // Submit ajax to remove the existing selected blockout
                    $.ajax({
                        url : url,
                        type : 'GET',
                        dataType : 'json',
                        success : function (result) {

                        },
                        complete : function () {
                            // $(element).removeClass('disabled');
                        }
                    });

                    return exists;
                }
            });

            return exists;
        },

        updateInput : function () {
            var events = [],
                data = '';

            for (var i = 0; i < $(this.options.calendar).clndr().options.events.length; i++) {
                events[i] = $(this.options.calendar).clndr().options.events[i].date.format('L');
            }

            data = JSON.stringify(events);
            $(this.options.input).val(data);
        }
    };

    return BlockoutFieldset;
});
