define([
    'language',
    'picker/date',
    'picker/time'
], function (Lang) {
    var BookingFieldset = function (options) {
        this.init(options);
    };

    BookingFieldset.defaults = {
        'date' : 'input[name="booking[date]"]',
        'hour' : 'input[name="booking[departure_hour]"]',
        'location' : 'input[name="booking[departure_location]"]',
        'adults' : 'select[name="booking[adults]"]',
        'childs' : 'select[name="booking[childs]"]',
        'datepicker' : {
            //
        },
        'timepicker' : {

        },
        'validate' : {
            rules : {
                'booking[date]' : 'required',
                'booking[adults]' : 'required',
                'booking[package_id]' : 'required'
            },
            messages : {

            }
        }
    };

    /* Public Methods */
    BookingFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, BookingFieldset.defaults, options);
            this.datepicker(this.options.datepicker);
            this.timepicker(this.options.timepicker);
            this.searchBox();
        },

        rules : function () {
            return this.options.validate.rules;
        },

        datepicker : function (options) {
            $(this.options.date).pickadate(options);
        },

        timepicker : function (options) {
            $(this.options.hour).pickatime(options);
        },

        searchBox : function () {
            $.each($(this.options.location), function () {
                var input = document.getElementById($(this).prop('id'));
                this.searchBox = new google.maps.places.SearchBox(input);

                $(this).on('keypress', function(event){
                    if (event.keyCode == 10 || event.keyCode == 13)
                        event.preventDefault();
                });
            });
        }
    };

    return BookingFieldset;
});
