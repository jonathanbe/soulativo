define([
    'language',
    'picker/date',
    'picker/time'
], function (Lang) {
    var BookingFieldset = function (options) {
        this.init(options);
    };

    BookingFieldset.defaults = {
        'date' : 'input[name="booking[date]"]',
        'hour' : 'input[name="booking[departure_hour]"]',
        'location' : 'input[name="booking[departure_location]"]',
        'adults' : 'select[name="booking[adults]"]',
        'childs' : 'select[name="booking[childs]"]',
        'datepicker' : {
            min: true
        },
        'timepicker' : {

        },
        'validate' : {
            rules : {
                'booking[date]' : 'required'
            },
            messages : {

            }
        }
    };

    /* Public Methods */
    BookingFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, BookingFieldset.defaults, options);
            this.datepicker(this.options.datepicker);
            this.timepicker(this.options.timepicker);
            this.searchBox();
        },

        rules : function () {
            return this.options.validate.rules;
        },

        datepicker : function (options) {
            if($('#blockouts').length > 0){
                var dates = $.parseJSON($('#blockouts').val()),
                    fixedDates = [];

                $.each(dates, function () {
                    fixedDates.push(new Date(this));
                });

                options.disable = fixedDates;
            }

            $(this.options.date).pickadate(options);
        },

        timepicker : function (options) {
            $(this.options.hour).pickatime(options);
        },

        disableDates : function () {

        },

        searchBox : function () {
            $.each($(this.options.location), function () {
                var input = document.getElementById($(this).prop('id'));
                this.searchBox = new google.maps.places.SearchBox(input);

                $(this).on('keypress', function(event){
                    if (event.keyCode == 10 || event.keyCode == 13)
                        event.preventDefault();
                });
            });
        }
    };

    return BookingFieldset;
});
