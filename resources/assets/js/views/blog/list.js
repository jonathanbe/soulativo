define([
    'paginate'
], function (Paginate) {
    return {
        init : function () {
            var that = this;

            this.pagination = new Paginate({
                url : $('.blog footer button.-load').data('url'),
                limit : 4,
                templateElement : '#post-loop-template'
            });

            $('.blog footer button.-load').on('click', function () {
                that.pagination.loadMore($(this).parent());
            });
        }
    }
});
