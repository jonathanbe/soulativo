define([
    'customer/fieldset',
    'address/fieldset',
    'form'
], function (Customer, Address, Form) {
    return {
        init : function () {
            this.customer = new Customer();
            this.address = new Address({
                customer : this.customer
            });
            this.validation = $.extend(true, {},
                this.customer.options.validate,
                this.address.options.validate,
                {
                    submitHandler : function () {
                        return true;
                    }
                }
            );
            this.form = new Form('form.checkout', {
                validate : this.validation
            });
        }
    }
});
