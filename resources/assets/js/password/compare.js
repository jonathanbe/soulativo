define([

], function () {
    var Compare = function (options) {
        this.init(options);
    };

    Compare.defaults = {
        form : '',
        field : 'input[name="password"]',
        confirm : 'input[name="password_confirmation"]',
        successClass : 'success',
        errorClass : 'danger'
    };

    /* Public Methods */
    Compare.prototype = {
        options : {},
        init : function (options) {
            that = this;
            this.options = $.extend(true, {}, Compare.defaults, options);

            $(this.options.field, that.options.form).on('keyup', function () { return that.passwordsCompare(that); } );
            $(this.options.confirm, that.options.form).on('keyup', function () { return that.passwordsCompare(that); } );
        },

        passwordsCompare : function (that) {
            if($(that.options.field, that.options.form).val() === $(that.options.confirm, that.options.form).val() &&
                $(that.options.field, that.options.form).val() != '')
                return that.passwordsMatch();
            else
                return that.passwordsFail();
        },

        passwordsMatch : function () {
            $(that.options.field, that.options.form).parent().addClass('has-' + that.options.successClass).removeClass('has-' + that.options.errorClass);
            $(that.options.confirm, that.options.form).parent().addClass('has-' + that.options.successClass).removeClass('has-' + that.options.errorClass);
            $(that.options.field, that.options.form).addClass('form-control-' + that.options.successClass).removeClass('form-control-' + that.options.errorClass);
            $(that.options.confirm, that.options.form).addClass('form-control-' + that.options.successClass).removeClass('form-control-' + that.options.errorClass);
            return true;
        },

        passwordsFail : function () {
            $(that.options.field, that.options.form).parent().addClass('has-' + that.options.errorClass).removeClass('has-' + that.options.successClass);
            $(that.options.confirm, that.options.form).parent().addClass('has-' + that.options.errorClass).removeClass('has-' + that.options.successClass);
            $(that.options.field, that.options.form).addClass('form-control-' + that.options.errorClass).removeClass('form-control-' + that.options.successClass);
            $(that.options.confirm, that.options.form).addClass('form-control-' + that.options.errorClass).removeClass('form-control-' + that.options.successClass);
            return false;
        }
    };

    return Compare;
});
