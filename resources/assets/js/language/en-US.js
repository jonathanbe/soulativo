define([], function () {
    return {
        validate : {
            address : {
                city : 'Enter your city',
                street : 'Enter your street'
            },
            customer : {
                birthday : 'Select your birthday',
                document : 'Enter your document number',
                number : 'Required'
            },
            user : {
                first_name : 'Enter your first name',
                last_name : 'Enter your last name'
            }
        }
    }
});
