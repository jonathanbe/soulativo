define([
    'password/compare',
    'form'
], function (Compare, Form) {

    var Register = function (element) {
        this.init(element);
    };

    Register.defaults = {
        validate : {
            rules : {
                'first_name' : 'required',
                'last_name' : 'required',
                'email' : {
                    'required' : true,
                    'email' : true
                },
                'password' : 'required'
            },
            messages : {

            }
        },
        callback : function(result) {
            window.location = '//' + window.location.host + result.redirect
        },
    };

    Register.prototype = {
        element : null,

        init : function (element) {
            var that = this;
            this.element = element;
            this.options = Register.defaults;
            this.options.validate.submitHandler = function () {
                that.signup();
                return false;
            };
            this.form = new Form(this.element, {
                validate : this.options.validate
            });
            this.compare = new Compare({
                form : this.form.element
            });
        },

        signup : function () {
            var that = this,
                formData = $(this.element).serialize(),
                url = $(this.element).prop('action');

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                success : function(result){
                    if(!result.error){
                        $(that.element).closest('.modal').modal('hide');
                        that.options.callback(result);
                    }else{
                        $('.alert', that.element).html(result.message);
                        $('.alert', that.element).css('opacity', 0);
                        $('.alert', that.element).slideDown(300, function() {
                            $(this).animate({ opacity: 1 }, 300);
                        });
                    }
                },
                complete : function(result){

                }
            });
        },

        socialSignup : function () {

        }
    };

    return Register;
});
