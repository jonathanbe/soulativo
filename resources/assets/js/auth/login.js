define([
    'form'
], function (Form) {

    var Login = function (element) {
        this.init(element);
    };

    Login.defaults = {
        validate : {
            rules : {
                'email' : {
                    'required' : true,
                    'email' : true
                },
                'password' : 'required'
            },
            messages : {

            }
        },
        callback : function(result) {
            window.location = '//' + window.location.host + result.redirect
        },
    };

    Login.prototype = {
        element : null,

        init : function (element) {
            var that = this;
            this.element = element;
            this.options = Login.defaults;
            this.options.validate.submitHandler = function () {
                that.signin();
                return false;
            };
            this.form = new Form(this.element, {
                validate : this.options.validate
            });
        },

        signin : function () {
            var that = this,
                formData = $(this.element).serialize(),
                url = $(this.element).prop('action');

            $.ajax({
                url : url,
                type : 'POST',
                data : formData,
                dataType : 'json',
                success : function(result){
                    if(!result.error){
                        $(that.element).closest('.modal').modal('hide');
                        that.options.callback(result);
                    }else{
                        $('.alert', that.element).css('opacity', 0);
                        $('.alert', that.element).slideDown(300, function() {
                            $(this).animate({ opacity: 1 }, 300);
                        });
                    }
                },
                complete : function(result){

                }
            });
        },

        socialSignin : function () {

        }
    };

    return Login;
});
