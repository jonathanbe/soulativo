define([
    'picker/date',
], function () {
    var Search = function (options) {
        this.init(options);
    };

    Search.defaults = {
        form : 'form.search',
        startdate : 'input[name="search[from]"]',
        enddate : 'input[name="search[to]"]'
    };

    Search.prototype = {
        init : function (options) {
            var that = this;
            this.options = $.extend(true, {}, Search.defaults, options);
            this.startDate();
            this.endDate();
        },

        startDate : function () {
            var that = this;
            $(this.options.startdate, this.options.form).pickadate({
                min: true
            });

            $(this.options.startdate, this.options.form).on('change', function(){
                var picker = $(this).pickadate('picker');
                // Set the min range to the end input
                $(that.options.enddate, that.options.form).pickadate('picker').set('min', new Date(picker.get('select', 'yyyy/mm/dd')));
            });
        },

        endDate : function () {
            var that = this;
            $(this.options.enddate, this.options.form).pickadate({
                min: true
            });

            $(this.options.enddate, this.options.form).on('change', function(){
                var picker = $(this).pickadate('picker');
                // Set the min range to the end input
                $(that.options.startdate, that.options.form).pickadate('picker').set('max', new Date(picker.get('select', 'yyyy/mm/dd')));
            });
        }
    };

    return Search;
});
