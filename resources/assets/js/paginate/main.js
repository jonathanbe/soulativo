define([

], function () {
	/* Private variables */
	var Paginate = function (options) {
			this.init(options);
		}, template = '';

    Paginate.defaults = {
		url : '',
        page : 2,
        limit : 3,
        templateElement : ''
	};

    Paginate.prototype = {
		/* Public methods */
		init : function(options){
			this.options = $.extend(true, {}, Paginate.defaults, options);
            template = _.template($(this.options.templateElement).html());
		},

        loadMore : function (target) {
            var that = this;

            $.ajax({
                url : '//' + location.host + '/' + this.options.url + '/' + this.options.page + '/' + this.options.limit,
                type : 'GET',
                dataType : 'json',
                success : function(result){
                    if(result.length > 0){
                        $.each(result, function () {
                            $(target).before(template(this));
                        });
                        that.options.page++;
                    }
                }
            });
        }
	};

	return Paginate;
});
