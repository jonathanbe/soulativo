define([
    'language',
    'jquery-mask'
], function (Lang) {
    var CustomerFieldset = function (options) {
        this.init(options);
    };

    CustomerFieldset.defaults = {
        'telephone' : 'input[name="customer[telephone]"]',
        'birthday' : 'input[name="customer[birthday]"]',
        'document' : 'input[name="customer[document]"]',
        'datepicker' : {
            //
        },
        'validate' : {
            rules : {
                'customer[birthday]' : 'required',
                'customer[document]' : 'required'
            },
            messages : {
                'customer[birthday]' : Lang.validate.customer.birthday,
                'customer[document]' : Lang.validate.customer.document
            }
        }
    };

    /* Public Methods */
    CustomerFieldset.prototype = {
        init : function (options) {
            this.options = $.extend(true, {}, CustomerFieldset.defaults, options);
            if($('select[name="address[country_id]"]').length > 0){
                this.createMasks($('select[name="address[country_id]"]').find(':selected').data('calling-code'));
            }else{
                this.createMasks();
            }
        },

        rules : function () {
            return this.options.validate.rules;
        },

        createMasks : function (callingCode) {
            this.destroyMasks();
            this.birthdayMask(callingCode);
            this.documentMask(callingCode);
            this.telephoneMask(callingCode);
        },

        destroyMasks : function () {
            $('input[name="customer[telephone]"]').unmask();
            $('input[name="customer[birthdate]"]').unmask();
            $('input[name="address[document]"]').unmask();
        },

        telephoneMask : function (code) {
          if(code == 55){
    		    var SPMaskBehavior = function (val) {
              if(val.replace(/\D/g, '').length === 11){
                  return '(00) 00000-0000';
              }else{
                  return '(00) 0000-00009';
              }
      			},
      			spOptions = {
      				onKeyPress: function (val, e, field, options) {
      					field.mask(SPMaskBehavior.apply({}, arguments), options);
      				}
      			};

            $(this.options.telephone).mask(SPMaskBehavior, spOptions);
          } else {
            $(this.options.telephone).unmask();
          }
    	},

        birthdayMask : function (code) {
            if(code == 55){
                $(this.options.birthday).mask('00/00/0000');
            }else{
                $(this.options.birthday).unmask();
            }
        },

        documentMask : function (code) {
            if(code == 55){
                $(this.options.document).mask('000.000.000-00');
            }else{
                $(this.options.document).unmask();
            }
        }
    };

    return CustomerFieldset;
});
