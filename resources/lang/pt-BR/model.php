<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'user' => [
        'singular' => 'Usuário',
        'plural' => 'Usuários',
        'first_name' => 'Nome',
        'last_name' => 'Sobrenome',
        'email' => 'E-mail',
        'role' => 'Permissão',
        'create' => 'Usuário criado com sucesso!',
        'edit' => 'Usuário editado com sucesso!',
        'password' => 'Senha',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'password' => [
        'reset' => 'Redefinir senha',
        'new' => 'Nova senha',
        'confirm' => 'Confirmar senha'
    ],
    'customer' => [
        'singular' => 'Cliente',
        'plural' => 'Clientes',
        'birthday' => 'Data de nascimento',
        'document' => 'Nº de CPF',
        'telephone' => 'Telefone',
        'card_brand' => 'Bandeira do cartão',
        'card_last_four' => 'Quatro últimos dígitos',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira',
        'saved' => "Salvo"
    ],
    'partner' => [
        'singular' => 'Parceiro',
        'plural' => 'Parceiros',
        'document' => 'Nº de documento',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'store' => [
        'singular' => 'Loja',
        'plural' => 'Lojas',
        'name' => 'Nome da loja',
        'slug' => 'Slug',
        'document' => 'Nº CNPJ',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'address' => [
        'singular' => 'Endereço',
        'plural' => 'Endereços',
        'state' => 'Estado',
        'city' => 'Cidade',
        'zipcode' => 'CEP',
        'street' => 'Rua',
        'number' => 'Número',
        'neighbourhood' => 'Bairro',
        'complement' => 'Complemento',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'country' => [
        'singular' => 'País',
        'plural' => 'Países',
        'select' => 'Selecione o país',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'product' => [
        'singular' => 'Produto',
        'plural' => 'Produtos',
        'content' => 'Conteúdo',
        'excerpt' => 'Resumo',
        'name' => 'Nome',
        'slug' => 'Slug',
        'price' => 'Preço',
        'store' => 'Loja',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'package' => [
        'singular' => 'Pacote',
        'plural' => 'Pacotes',
        'destination' => 'Destino',
        'terms' => 'Regulamento',
        'category' => 'Categoria',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'page' => [
        'singular' => 'Página',
        'plural' => 'Páginas',
        'title' => 'Título',
        'content' => 'Conteúdo',
        'excerpt' => 'Resumo',
        'slug' => 'Slug',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'gallery' => [
        'singular' => 'Galeria de Imagens',
        'plural' => 'Galerias de Imagens',
        'album' => 'Album',
        'thumbnail' => 'Imagem destacada'
    ],
    'category' => [
        'singular' => 'Categoria',
        'plural' => 'Categorias',
        'name' => 'Nome',
        'slug' => 'Slug',
        'parent' => 'Categoria Mãe',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'booking' => [
        'singular' => 'Reserva',
        'plural' => 'Reservas',
        'reference' => 'Voucher',
        'date' => 'Data',
        'adults' => 'Adultos',
        'childs' => 'Crianças',
        'departure_location' => 'Local de saída',
        'departure_hour' => 'Hora da saída',
        'status' => 'Status',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'blockout' => [
        'singular' => 'Bloqueio',
        'plural' => 'Bloqueios',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'dateformat' => [
        'full' => 'd/m/Y H:i',
        'short' => 'd/m/Y'
    ],
    'order' => [
        'singular' => 'Pedido',
        'plural' => 'Pedidos',
        'total' => 'Total',
        'created_at' => 'Gerado em',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'promotion' => [
        'singular' => 'Promoção',
        'plural' => 'Promoções',
        'name' => 'Nome',
        'value' => 'Valor',
        'from' => 'De',
        'to' => 'Até',
        'rules' => 'Regras',
        'trash' => 'Lixeira',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'post' => [
        'singular' => 'Post',
        'plural' => 'Posts',
        'title' => 'Title',
        'slug' => 'Slug',
        'excerpt' => 'Excerpt',
        'content' => 'Content',
        'category' => 'Categoria',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'seo' => [
        'title' => 'Título',
        'keywords' => 'Keywords',
        'description' => 'Descrição',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ],
    'coupon' => [
        'singular' => 'Cupom',
        'plural' => 'Cupons',
        'name' => 'Nome',
        'from' => 'Válido a partir de',
        'to' => 'Até',
        'key' => 'Código',
        'status' => 'Status',
        'unique' => 'Único uso',
        'multiple' => 'Múltiplo uso',
        'type' => 'Tipo',
        'value' => 'Valor',
        'create' => 'Cupom criado com sucesso!',
        'edit' => 'Cupom editado com sucesso!',
        'create' => 'Cadastrar',
        'edit' => 'Editar',
        'trash' => 'Lixeira'
    ]

];
