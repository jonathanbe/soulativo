<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'menu' => [
        'currency' => [
            'real' => 'Currency – Real (R$)',
            'dollar' => 'Currency – Dollar ($)',
            'euro' => 'Currency – Euro (€)',
        ],
        'lang' => [
            'current' => 'English version',
            'english' => 'English version',
            'portuguese' => 'Versão em português',
            'spanish' => 'Version en español',
        ],
        'partner' => 'Become a partner',
    ],
    'categories' => [
        'adventure' => 'Adventure',
        'city-tour' => 'City Tour',
        'transport' => 'Transport',
    ],
    'profile' => [
        'title' => 'Profile',
        'user' => 'Access data',
        'address' => 'Billing address',
        'customer' => 'Personal data',
        'orders' => 'Recent orders'
    ],
    'auth' => [
        'title' => 'Road to adventure!',
        'subtitle' => 'Sign in to proceed',
        'signin' => 'Sign in',
        'signout' => 'Logout',
        'login' => [
            'title' => 'Already have an account',
            'submit' => 'Sign in',
            'password' => 'Aff, esqueci a senha',
            'error' => 'Wrong information',
        ],
        'register' => [
            'title' => 'Not member yet',
            'submit' => 'Sign up',
        ]
    ],
    'social' => [
        'facebook' => [
            'share' => 'Share on Facebook',
            'signin' => 'Sign in with Facebook',
        ],
        'twitter' => [
            'share' => 'Share on Twitter',
            'signin' => 'Sign in with Twitter'
        ],
    ],
    'cart' => [
        'bag' => [
            'singular' => 'item in the bag',
            'plural' => 'items in the bag',
            'empty' => 'Your bag is empty',
        ]
    ],
    'search' => [
        'title' => 'Find your adventure',
        'experience' => [
            'title' => 'What is your experience?',
            'beginner' => 'Beginner',
            'radical' => 'Radical',
        ],
        'submit' => 'Search'
    ],
    'booking' => [
        'when' => 'Book now',
        'many' => 'How many people?',
        'from' => 'From:',
        'to' => 'Until:',
        'each' => 'each',
        'reservation' => [
            'singular' => 'reservation',
            'plural' => 'reservations',
        ],
        'adult' => [
            'singular' => 'adult',
            'plural' => 'adults',
        ],
        'child' => [
            'empty' => 'No children',
            'singular' => 'child',
            'plural' => 'children',
        ],
        'pickup' => [
            'label' => 'Pick up (optional)',
            'location' => 'Where will you be?',
            'hour' => 'Enter an hour',
        ]
    ],
    'package' => [
        'per' => [
            'adult' => 'per adult',
            'child' => 'per child'
        ],
        'featured' => [
            'more' => 'Dive in',
        ],
        'tabs' => [
            'content' => 'Your adventure',
            'terms' => 'Terms',
            'map' => 'Map',
        ],
        'parcel' => '',
        'related' => 'See also',
        'submit' => [
            'regular' => 'Buy now',
            'discounted' => 'Buy now with :percent% off',
        ],
        'contact-price' => 'Consult the Price'
    ],
    'product' => [
        'discount' => ':percent% off!',
    ],
    'checkout' => [
        'cart' => [
            'step' => 'Step 1 of 3',
            'title' => 'Bag',
            'description' => 'verify the amount of items in your bag and<br /> confirm the schedule dates.',
            'back' => 'Back to shopping',
            'remove' => 'Remove from bag',
        ],
        'customer' => [
            'step' => 'Step 2 of 3',
            'title' => 'Additional data',
            'description' => 'We need you to fill some additional data to<br /> procceed with your order. Will be really quickly.',
            'back' => 'Back',
        ],
        'payment' => [
            'step' => 'Last step',
            'title' => 'Payment',
            'description' => 'Choose a payment method which is best for you.<br /> This environment is 100% safe.',
            'back' => 'Back',
            'summary' => 'Summary',
            'method' => [
                'title' => 'Payment method',
                'card' => 'Pay with credit card',
                'paypal' => 'Pay with PayPal',
                'pagseguro' => 'Pay with PagSeguro',
                'mercadopago' => 'Pay with MercadoPago',
            ],
            'submit' => 'Pay',
        ],
        'confirmation' => [
            'step' => 'Thank you!',
            'title' => 'Payment Sent',
            'description' => 'Your payment was sent. You can follow the order<br /> status through <a href=":link">Profile</a> section',
            'back' => 'Back to shopping',
        ],
        'submit' => 'Next step',
    ],
    'order' => [
        'pickup' => 'Pickup at',
        'list' => [
            'date' => 'Paid on',
        ],
        'status' => [
            'pending' => 'Waiting approving',
            'approved' => 'Confirmed',
            'rejected' => 'Rejected',
        ],
    ],
    'blog' => [
        'latest' => 'Latest on the blog',
        'more' => 'See blog'
    ],
    'newsletter' => [
        'placeholder' => 'Your email',
        'footer' => [
            'title' => 'Sign up and receive exclusive content',
            'submit' => 'Sign up',
        ],
        'aside' => [
            'title' => 'Sign up and receive exclusive content',
            'submit' => 'Sign up',
        ]
    ],
    'contact' => [
        'title' => 'Contact',
        'description' => 'Questions about anything? No problems. Contact us through the form bellow, telephone, Facebook, Whatsapp, Twitter, etc.',
        'form' => [
            'title' => 'Talk with us',
            'subject' => 'Subject',
            'message' => 'Message',
            'submit' => 'Send message',
        ],
        'aside' => [
            'title' => 'Find us also on',
            'faq' => 'See our FAQ',
        ]
    ],
    'payment' => [
        'title' => 'Payment methods'
    ],
    'copyright' => 'Copyright 2016 SoulAtivo'
];
