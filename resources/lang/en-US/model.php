<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fieldset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'user' => [
        'singular' => 'User',
        'plural' => 'Users',
        'first_name' => 'Name',
        'last_name' => 'Last name',
        'email' => 'Email',
        'role' => 'Permission',
        'create' => 'User sucessfully created!',
        'edit' => 'User sucessfully edited!',
        'password' => 'Password',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'password' => [
        'reset' => 'Reset password',
        'new' => 'New password',
        'confirm' => 'Confirm password'
    ],
    'customer' => [
        'singular' => 'Customer',
        'plural' => 'Customers',
        'birthday' => 'Birthday',
        'document' => 'Document number',
        'telephone' => 'Phone number',
        'card_brand' => 'Card brand',
        'card_last_four' => 'Last four digits',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash',
        'saved' => "Salvo"
    ],
    'partner' => [
        'singular' => 'Partner',
        'plural' => 'Partners',
        'document' => 'Document number',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'store' => [
        'singular' => 'Store',
        'plural' => 'Stores',
        'name' => 'Store name',
        'slug' => 'Slug',
        'document' => 'Document number',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'address' => [
        'singular' => 'Address',
        'plural' => 'Addresses',
        'state' => 'State',
        'city' => 'City',
        'zipcode' => 'Zipcode',
        'street' => 'Street',
        'number' => 'Number',
        'neighbourhood' => 'Neighbourhood',
        'complement' => 'Complement',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'country' => [
        'singular' => 'Country',
        'plural' => 'Countries',
        'select' => 'Select country',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'product' => [
        'singular' => 'Product',
        'plural' => 'Products',
        'content' => 'Content',
        'excerpt' => 'Excerpt',
        'name' => 'Name',
        'slug' => 'Slug',
        'price' => 'Price',
        'store' => 'Store',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'package' => [
        'singular' => 'Package',
        'plural' => 'Packages',
        'destination' => 'Destination',
        'terms' => 'Terms',
        'category' => 'Category',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'page' => [
        'singular' => 'Page',
        'plural' => 'Pages',
        'title' => 'Title',
        'content' => 'Content',
        'excerpt' => 'Excerpt',
        'slug' => 'Slug',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'gallery' => [
        'singular' => 'Gallery',
        'plural' => 'Gallery',
        'album' => 'Album',
        'thumbnail' => 'Thumbnail'
    ],
    'category' => [
        'singular' => 'Category',
        'plural' => 'Categories',
        'name' => 'Name',
        'slug' => 'Slug',
        'parent' => 'Parent',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'booking' => [
        'singular' => 'Booking',
        'plural' => 'Bookings',
        'reference' => 'Voucher',
        'date' => 'Date',
        'adults' => 'Adults',
        'childs' => 'Children',
        'departure_location' => 'Departure Location',
        'departure_hour' => 'Departure Hour',
        'status' => 'Status',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'blockout' => [
        'singular' => 'Blockout',
        'plural' => 'Blockouts',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'dateformat' => [
        'full' => 'Y-m-d H:i',
        'short' => 'Y-m-d'
    ],
    'order' => [
        'singular' => 'Order',
        'plural' => 'Orders',
        'total' => 'Total',
        'created_at' => 'Created at',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'promotion' => [
        'singular' => 'Promotion',
        'plural' => 'Promotions',
        'name' => 'Name',
        'value' => 'Value',
        'from' => 'From',
        'to' => 'To',
        'rules' => 'Rules',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'post' => [
        'singular' => 'Post',
        'plural' => 'Posts',
        'title' => 'Título',
        'slug' => 'Slug',
        'excerpt' => 'Resumo',
        'content' => 'Conteúdo',
        'category' => 'Category',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'seo' => [
        'title' => 'Title',
        'keywords' => 'Keywords',
        'description' => 'Description',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ],
    'coupon' => [
        'singular' => 'Coupon',
        'plural' => 'Coupons',
        'name' => 'Name',
        'from' => 'Valid from',
        'to' => 'To',
        'key' => 'Code',
        'status' => 'Status',
        'unique' => 'Unique',
        'multiple' => 'Multiple',
        'type' => 'Type',
        'value' => 'Value',
        'create' => 'Coupon sucessfully created!',
        'edit' => 'Coupon sucessfully edited!',
        'create' => 'Create',
        'edit' => 'Edit',
        'trash' => 'Trash'
    ]

];
