@extends('layouts.app')

@section('content')

@if(@$customer)
    {!! Form::model($customer, array(
        'method' => 'post',
        'class' => '-form-default',
        'action' => ['CustomerController@update']
    )) !!}
@else
    {!! Form::open(array('url' => '/customer/profile/store', 'class' => '-form-default')) !!}
@endif
    <input type="hidden" name="edit-profile" value="1" />
    <header class="-page-head" style="background-image: url(/images/delete/bg-top-01.jpg)">
        <div class="container">
            <div class="wrapper">
                <nav class="category">
                     <a href="/customer/profile">Cadastro »</a>
                </nav>
                <h1>Edite seu cadastro</h1>
            </div>
        </div>
    </header>
    
    <div class="customer -edit container">
        @include('admin.partials.flash')
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            @include('user.fieldset')
            @include('address.fieldset')
            @include('customer.fieldset')
        </div>

        <footer class="col-xs-12 col-md-6 col-md-offset-3">
            <input type="submit" name="name" class="-block" value="Salvar alterações">
        </footer>
    </div>
{!! Form::close() !!}

@endsection
