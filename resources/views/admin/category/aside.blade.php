<aside id="{{$id}}" data-action="/admin/{{$root->slug}}/category/create/aside" class="category card">
    <header class="card-header">
        {{$title}}
    </header>
    <div class="wrapper">
        <table class="table table-striped table-sm m-b-0">
            <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            <label class="m-l-{{$category->getLevel() - 1}}">
                                <input type="checkbox" id='category-{{$category->id}}' name='{{$name}}' class="m-l-1 m-r-1" data-parent="{{ ($category->getLevel() == 1) ? 0 : $category->parent_id}}" value="{{$category->id}}"
                                    @if($selected != null)
                                        @foreach($selected as $item)
                                            @if($item->id == $category->id)
                                                checked="checked"
                                            @endif
                                        @endforeach
                                    @endif
                                >
                                {{$category['name']}}
                            </label>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    <footer class="card-footer">
        <a href="#">+ Add new category</a>
        <div class="hidden-xs-up p-t-1">
            <div class="form-group">
                <input type="text" class="form-control" id="" placeholder="Category name">
            </div>
            <div class="form-group">
                <select class="form-control c-select">
                    <option value="{{$root->id}}" selected>Parent category</option>
                    @foreach($categories as $id => $category)
                        <option value="{{$category->id}}">
                            @for ($i = 0; $i < $category->getLevel(); $i++)–@endfor
                            {{$category->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Add new category</button>
        </div>
    </footer>
</aside>
