@extends('admin.layouts.app')

@section('content')
    {!! Form::model($store, array(
        'method' => 'post',
        'id' => 'form-store',
        'action' => ['Admin\StoreController@update', $store->id]
    )) !!}

        @include('admin.store.form', ['submitButton' => 'Update', 'store' => $store])

    {!! Form::close() !!}
@endsection
