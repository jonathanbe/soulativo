/@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/store/create',
        'id' => 'form-store'
    )) !!}

        @include('admin/store/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
