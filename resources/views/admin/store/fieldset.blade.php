<fieldset id="user">
    <legend>@lang('model.store.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="user-first_name">@lang('model.store.name')</label>
            {!! Form::text('store[name]', null, ['id' => 'store-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="user-last_name">@lang('model.store.slug')</label>
            {!! Form::text('store[slug]', null, ['id' => 'store-slug', 'class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="user-email">@lang('model.store.document')</label>
        {!! Form::text('store[document]', null, ['id' => 'store-document', 'class' => 'form-control']) !!}
        {!! Form::hidden('store[address_id]', null, ['id' => 'store-address_id']) !!}
    </div>

</fieldset>
