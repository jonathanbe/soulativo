<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.store.fieldset')
    @include('admin.address.fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
</aside>
