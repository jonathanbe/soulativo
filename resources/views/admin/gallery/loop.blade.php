<figure class="col-xs-6 col-md-4 col-xl-3" data-id="{{ $media->id }}" data-filename="{{ $media->filename }}" data-title="{{ $media->title }}" data-created="{{ $media->created_at }}" data-size="{{ $media->size }}" data-width="0" data-height="0" data-caption="{{ $media->caption }}">
    <span class="fa fa-check-square"></span>
    <span class="fa fa-minus-square"></span>
    <img src="/images/{{ $media->filename }}?w=150&h=150&fit=crop-50-50" alt="{{ $media->title }}">
</figure>
