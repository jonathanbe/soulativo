<fieldset class="gallery thumbnail card">
    <header class="card-header">
        @lang('model.gallery.thumbnail')
    </header>
    <div id="thumbnail-wrapper" class="card-block">
        @if($media)
            @include('admin.gallery.loop', ['media' => $media])
        @endif
    </div>
    <footer class="card-footer">
        <a href="#thumbnail-modal" id="thumbnail-set" data-toggle="modal">+ Set @lang('model.gallery.thumbnail')</a>
    </footer>
    {!! Form::hidden($name, null, ['class' => 'gallery-json form-control']) !!}
</fieldset>
