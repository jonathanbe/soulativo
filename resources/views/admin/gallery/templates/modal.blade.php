<script type="text/template" id="gallery-modal-template">
    <div class="gallery modal fade" id="<%= indent %>" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">Gallery</h4>
                </div>

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#gallery-upload-<%= index %>">Upload a file</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#gallery-album-<%= index %>">Media library</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="gallery-upload-<%= index %>" class="modal-body row tab-pane">
                      <form action="/media/upload" class="upload col-xs-12 col-xl-8">
                          <h3>Upload a file</h3>
                          <input type="file" multiple="1" name="files">
                      </form>
                    </div>

                    <div id="gallery-album-<%= index %>" class="modal-body row tab-pane active">
                        <div class="album-wrapper">
                            <div class="col-xs-12 col-md-8">
                                <div class="album row" data-multiple="<%= multiple %>">
                                    <!-- Photos -->
                                </div>
                            </div>
                            <aside class="sidebar col-xs-12 hidden-xs-down col-md-4">
                                <!-- Details -->
                            </aside>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="submit btn btn-primary" data-dismiss="modal">Set images</button>
                </div>
            </div>
        </div>
    </div>
</script>
