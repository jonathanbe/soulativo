<fieldset id="seo">
    <legend>SEO</legend>
    <div class="form-group">
        <label class="text-help" for="">@lang('model.seo.title')</label>
        {!! Form::text('seo[title]', null, ['id' => 'seo-title', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('model.seo.keywords')</label>
        {!! Form::text('seo[keywords]', null, ['id' => 'seo-keywords', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('model.seo.description')</label>
        {!! Form::textarea('seo[description]', null, ['id' => 'seo-description', 'class' => 'form-control']) !!}
    </div>
</fieldset>
