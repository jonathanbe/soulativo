@extends('admin.layouts.app')

@section('content')
    @if(@empty($seo))
        {!! Form::open(array(
            'url' => 'admin/seo/' . $slug . '/create/' . $id,
            'id' => 'form-post',
            'method' => 'post'
        )) !!}
    @else
        {!! Form::model($seo, array(
            'method' => 'post',
            'id' => 'form-seo',
            'action' => ['Admin\SEOController@update', $slug, $id]
        )) !!}
    @endif

        @include('admin.seo.form', ['submitButton' => 'Update', 'seo' => $seo])

    {!! Form::close() !!}
@endsection
