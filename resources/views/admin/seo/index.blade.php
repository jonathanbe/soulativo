@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">SEO</a>
            @include('admin.search.form')
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#posts">Posts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#pages">Pages</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#categories">Categories</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#packages">Packages</a>
            </li>
        </ul>

        <div class="tab-content m-t-1">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'posts', 'class' => 'tab-pane active')) !!}
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                Post name
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>
                                    <a href="{{ url('admin/seo/post/edit/' . $post->id)}}">{{$post->title}}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/post" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'pages', 'class' => 'tab-pane')) !!}
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                Page name
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>
                                    <a href="{{ url('admin/seo/page/edit/' . $page->id) }}">{{$page->title}}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/page" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'categories', 'class' => 'tab-pane')) !!}
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                Category name
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $id => $category)
                            <tr>
                                <td>
                                    <a href="{{ url('admin/seo/cat/edit/' . $id) }}">{{$category}}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/category" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'packages', 'class' => 'tab-pane')) !!}
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                Package name
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td>
                                    <a href="{{ url('admin/seo/package/edit/' . $package->id) }}">{{$package->product->name}}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/package" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/seo/templates/post-list')
    @include('admin/seo/templates/page-list')
    @include('admin/seo/templates/category-list')
    @include('admin/seo/templates/package-list')
@endsection
