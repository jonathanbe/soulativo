<script type="text/template" id="category-list-template">
    <% for(var category in result) { %>
        <tr>
            <td>
                <a href="{{ url('admin/seo/category/edit') }}/<%= result[category].id %>">
                    <% for(i = 0; i < result[category].level - 1; i++) { %>-<% } %>
                    <%= result[category].name %>
                </a>
            </td>
        </tr>
    <% } %>
</script>
