<fieldset id="blockout">
    <legend>@lang('model.blockout.plural') - {{$package->product->name}}</legend>
    <div id="calendar"></div>
    {!! Form::hidden('package[blockouts]', $blockouts, ['id' => 'package-blockouts', 'class' => 'form-control', 'data-id' => $package->id]) !!}
</fieldset>
