@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.blockout.plural')</a>
            @include('admin.search.form')
        </nav>

        {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th>
                            <input type="checkbox" class="m-r-1" value=""> @lang('model.package.singular')
                        </th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($packages as $package)
                    <tr>
                        <td>
                            <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$package->id}}">
                            <a href="{{ url('admin/blockout/show/package/'.$package->id) }}">{{$package->product->name}}</a>
                        </td>
                        <td><small>{{ isset($package->created_at) ? $package->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <nav>
                <ul class="pager">
                    <li><a href="/blockout" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                </ul>
            </nav>
        {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/blockout/templates/list')
@endsection
