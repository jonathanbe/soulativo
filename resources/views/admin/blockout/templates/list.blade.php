<script type="text/template" id="blockout-list-template">
    <% for(var blockout in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[blockout].id %>">
                <a href="{{ url('admin/blockout/show/package') }}/<%= result[blockout].id %>"><%= result[blockout].name %></a></td>
            <td><small><%= result[blockout].date %></small></td>
        </tr>
    <% } %>
</script>
