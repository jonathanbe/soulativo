<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.booking.fieldset')
    @if(@count($booking->order) > 0)
        @include('admin.order.invoices', ['order' => $booking->order])
    @endif
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
    @include('admin.booking.aside')
    @include('admin.order.aside')
</aside>
