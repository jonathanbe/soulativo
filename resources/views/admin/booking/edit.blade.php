@extends('admin.layouts.app')

@section('content')
    {!! Form::model($booking, array(
        'method' => 'post',
        'action' => ['Admin\BookingController@update', $booking->id],
        'id' => 'form-booking'
    )) !!}

        @include('admin.booking.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}

    @include('admin.order.modal', ['order' => $booking->order])
@endsection
