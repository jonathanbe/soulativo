@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/booking/create',
        'id' => 'form-booking',
        'method' => 'post'
    )) !!}

        @include('admin/booking/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
