@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.booking.plural') <a href="{{ url('/admin/booking/create') }}" class="btn btn-primary">Add new @lang('model.booking.singular')</a></a>
            @include('admin.search.form', ['action' => '/booking/search'])
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($bookings)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_bookings)}})</a>
            </li>
        </ul>

        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/booking/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> Ref
                            </th>
                            <th>@lang('model.customer.singular')</th>
                            <th>@lang('model.package.singular')</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($bookings as $booking)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="1">
                                    <a href="{{ url('admin/booking/edit/' . $booking->id) }}">#{{$booking->id}}</a>
                                </td>
                                <td><a href="{{ url('admin/customer/edit/' . $booking->order->customer->id) }}">{{$booking->order->customer->user->name}}</a></td>
                                <td><a href="{{ url('admin/package/edit/' . $booking->package->id) }}">{{$booking->package->product->name}}</a></td>
                                <td>
                                    @if($booking->status == 2)
                                        <span class="label label-success">Confirmed</span>
                                    @elseif($booking->status == 3)
                                        <span class="label label-info">Rejected</span>
                                    @else
                                        <span class="label {{$booking->pendingClass}}">Pending</span>
                                    @endif
                                </td>
                                
                            </tr>
                            
                        @endforeach
                        
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/booking" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
            <input type="hidden" name="trash_delete" id="trash_delete" value="1">
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/booking/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/booking/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_bookings) === 0) hidden-xs-up @endif"">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> Name
                            </th>
                            <th>E-mail</th>
                            <th>Last login</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($deleted_bookings as $dbooking)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="1">
                                    <a href="{{ url('admin/booking/edit/' . $dbooking->id) }}">#{{$dbooking->id}}</a>
                                </td>
                                <td><a href="{{ url('admin/customer/edit/' . $dbooking->order->customer->id) }}">{{$dbooking->order->customer->user->name}}</a></td>
                                <td><a href="{{ url('admin/package/edit/1' . $dbooking->package->id) }}">{{$dbooking->package->product->name}}</a></td>
                                <td>
                                    @if($dbooking->status == 2)
                                        <span class="label label-success">Confirmed</span>
                                    @elseif($dbooking->status == 3)
                                        <span class="label label-info">Rejected</span>
                                    @else
                                        <span class="label {{$dbooking->pendingClass}}">Pending</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (count($deleted_bookings) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/booking/templates/list')
@endsection
