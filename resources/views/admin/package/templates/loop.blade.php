<script type="text/template" id="package-loop-template">
    <header style="<% if(thumbnail != null) { %> background-image: url(/images/<%= thumbnail.filename %>) <% } %>">
        <button class="fa fa-pencil" data-toggle="modal" data-target="#select-package"></button>
    </header>
    <div class="card-block">
        <h4 class="card-title"><%= product.name %></h4>
    </div>
</script>
