<script type="text/template" id="package-list-template">
    <% for(var package in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[package].id %>">
                <a href="{{ url('admin/package/edit') }}/<%= result[package].id %>"><%= result[package].product.name %></a></td>
            <td>
                <% if(result[package].categories != null) { %>
                    <% for(var category in result[package].categories) { %>
                        <a href="admin/packages/category/edit/<%= result[package].categories[category].id %>" title="<%= result[package].categories[category].name %>"><%= result[package].categories[category].name %></a>
                    <% } %>
                <% } %>
            </td>
            <td><button data-id="<%= result[package].id %>" class="-featured fa <% if(result[package].featured){ %> -active fa-star <% }else{ %> fa-star-o <% } %>"></button></td>
        </tr>
    <% } %>
</script>
