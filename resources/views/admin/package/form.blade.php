<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.product.fieldset')
    @include('admin.package.fieldset')
    @include('admin.gallery.fieldset', [
        'id' => 'package-gallery',
        'name' => 'package[gallery]',
        'medias' => @$gallery
    ])
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
    @include('admin.product.aside', ['product' => @$package->product])
    @include('admin.category.aside',
        [
            'id' => 'category-sidebar',
            'name' => 'product[categories][]',
            'root' => $root,
            'title' => trans('model.package.category'),
            'selected' => @$package->product->categories
        ]
    )
    @include('admin.gallery.thumbnail', [
        'id' => 'package-thumbnail_modal',
        'media' => @$package->thumbnail,
        'name' => 'package[thumbnail_id]'
    ])
</aside>
