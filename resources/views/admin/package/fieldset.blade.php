<fieldset class="package package--form" id="package">
    <legend>
      <label class="package__adults-only form-control custom-checkbox">
          <input type="checkbox" id='package-only_adults' name='package[adults_only]' class="custom-control-input" value="1" {{ (@$package['adults_only'] == true) ? 'checked="checked"' : '' }}>
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Apenas adultos</span>
      </label>
      <label class="package__unity form-control custom-checkbox">
          <input type="checkbox" id='package-unity' name='package[unity]' class="custom-control-input" value="1" {{ (@$package['unity'] == true) ? 'checked="checked"' : '' }}>
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Apenas Unidades</span>
      </label>
      @lang('model.package.singular')
    </legend>
    <div class="row">
      <div class="form-group col-xs-12 col-md-6">
          <label class="text-help" for="package-destination">@lang('model.package.destination')</label>
          {!! Form::text('package[destination]', null, ['id' => 'package-destination', 'class' => 'form-control']) !!}
      </div>
      <div class="form-group col-xs-12 col-md-6">
          <label class="text-help" for="product-name">Ordem:</label>
          {!! Form::text('package[order]', null, ['id' => 'package-order', 'class' => 'form-control']) !!}
      </div>
    </div>
    <div class="form-group">
        <div id="package-destination-map" class="google-map"></div>
        {!! Form::hidden('package[map_data]', null, ['id' => 'package-map_data', 'class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label class="text-help" for="package-terms">@lang('model.package.terms')</label>
        {!! Form::hidden('package[terms]', null, ['id' => 'package-content', 'class' => 'form-control']) !!}
        <div class="-dante">
            <div id="terms-editor" class="-content">
                @if(old('package')['terms'])
                    {!!old('package')['terms']!!}
                @else
                    {!!@$package[terms]!!}
                @endif
            </div>
        </div>
    </div>
    {!! Form::hidden('package[id]', null, ['id' => 'package-id']) !!}
</fieldset>
