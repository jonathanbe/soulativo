@extends('admin.layouts.app')

@section('content')
    {!! Form::model($package, array(
        'method' => 'post',
        'action' => ['Admin\PackageController@update', $package->id],
        'id' => 'form-package'
    )) !!}

        @include('admin.package.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
