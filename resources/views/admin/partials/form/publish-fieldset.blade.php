<fieldset class="card">
    <header class="card-header">
        Publish
    </header>
    <div class="card-block">
        <p>Publicado em: <strong><a href="#">6 nov, 2015</a> at <a href="#">19:34</a></strong></p>
        {!! Form::submit($submitButton, ['class' => 'btn btn-primary']) !!}
    </div>
</fieldset>
