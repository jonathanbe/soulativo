@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	</div>
@endif

@if(Session::has('status'))
	<div class="alert alert-success">
		<p>{{ Session::get('status') }}</p>
	</div>
@endif
