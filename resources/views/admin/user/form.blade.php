<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.user.fieldset')
    @include('admin.role.user-fieldset')
    @include('admin.auth.passwords.reset-fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
</aside>
