@extends('admin.layouts.app')

@section('content')
    {!! Form::model($user, array(
        'method' => 'post',
        'action' => ['Admin\UserController@update', $user->id],
        'id' => 'form-user'
    )) !!}

        @include('admin.user.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
