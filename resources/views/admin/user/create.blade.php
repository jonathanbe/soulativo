@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/user/create',
        'id' => 'form-user',
        'method' => 'post'
    )) !!}

        @include('admin/user/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
