<fieldset id="user">
    <legend>@lang('model.user.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="user-first_name">@lang('model.user.first_name')</label>
            {!! Form::text('user[first_name]', null, ['id' => 'user-first_name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="user-last_name">@lang('model.user.last_name')</label>
            {!! Form::text('user[last_name]', null, ['id' => 'user-last_name', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="user-email">@lang('model.user.email')</label>
        {!! Form::email('user[email]', null, ['id' => 'user-email', 'class' => 'form-control', 'placeholder' => 'example@gmail.com']) !!}
    </div>
</fieldset>
