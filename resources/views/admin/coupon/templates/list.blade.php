<script type="text/template" id="coupon-list-template">
    <% for(var coupon in result) { %>
        <tr>
            <td>
                <input type="checkbox" name="checkbox[]" class="m-r-1" value="<%= result[coupon].id %>">
                <a href="{{ url('admin/coupon/edit') }}/<%= result[coupon].id %>"><%= result[coupon].name %></a></td>
            <td><small><%= result[coupon].created_at %></small></td>
        </tr>
    <% } %>
</script>
