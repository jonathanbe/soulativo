@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/coupon/create',
        'id' => 'form-page',
        'method' => 'post'
    )) !!}

        @include('admin/coupon/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
