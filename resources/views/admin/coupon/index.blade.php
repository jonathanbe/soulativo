@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.coupon.plural') <a href="{{ url('/admin/coupon/create') }}" class="btn btn-primary">Add new @lang('model.coupon.singular')</a></a>
            @include('admin.search.form', ['action' => '/coupon/search'])
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($coupons)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_coupons)}})</a>
            </li>
        </ul>

        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/coupon/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.coupon.name')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($coupons as $coupon)
                        <tr>
                            <td>
                                <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$coupon->id}}">
                                <a href="{{ url('admin/coupon/edit/'.$coupon->id) }}">{{$coupon->name}}</a>
                            </td>
                            <td><small>{{ isset($coupon->created_at) ? $coupon->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/coupon" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
            <input type="hidden" name="trash_delete" id="trash_delete" value="1">
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/coupon/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/coupon/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_coupons) === 0) hidden-xs-up @endif">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.coupon.title')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($deleted_coupons as $deleted_coupon)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$deleted_coupon->id}}">
                                    <a href="{{ url('admin/coupon/edit/'.$deleted_coupon->id) }}">{{$deleted_coupon->name}}</a></td>
                                <td><small>{{ isset($deleted_coupon->created_at) ? $deleted_coupon->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (count($deleted_coupons) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/coupon/templates/list')
@endsection
