<fieldset id="package">
    <legend>@lang('model.coupon.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="coupon-name">@lang('model.coupon.name')</label>
            {!! Form::text('coupon[name]', null, ['id' => 'coupon-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="coupon-slug">@lang('model.coupon.key') <small>(Opcional)</small></label>
            {!! Form::text('coupon[key]', null, ['id' => 'coupon-key', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="coupon-name">@lang('model.coupon.from')</label>
            {!! Form::text('coupon[from]', null, ['id' => 'coupon-name', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="coupon-slug">@lang('model.coupon.to')</label>
            {!! Form::text('coupon[to]', null, ['id' => 'coupon-key', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="coupon-value">@lang('model.coupon.value')</label>
            <div class="input-group">
                {!! Form::text('coupon[value]', null, ['id' => 'coupon-value', 'class' => 'form-control']) !!}
                <div class="input-group-addon">%</div>
            </div>
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="coupon-value">@lang('model.coupon.status')</label>
            <div class="radio">
                <label class="col-xs-12 col-md-6">
                    {!! Form::radio('coupon[status]', 0, null, ['id' => 'coupon-status-unique']) !!}
                    Único uso
                </label>
                <label class="col-xs-12 col-md-6">
                    {!! Form::radio('coupon[status]', 1, null, ['id' => 'coupon-status-multiple']) !!}
                    Múltiplo uso
                </label>
            </div>
        </div>
    </div>
    <div class="row">

        {!! Form::hidden('coupon[type]', 0, ['id' => 'coupon-type', 'class' => 'form-control']) !!}
    </div>
</fieldset>
