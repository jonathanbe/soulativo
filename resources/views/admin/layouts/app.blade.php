<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link  href="/backend/css/app.css" rel="stylesheet">

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <script type="text/javascript">
        window.config = {
            locale : '{{ config('app.locale') }}'
        }
    </script>
</head>
<body id="app-layout" data-view="@if(!empty($frontend['view'])){{ $frontend['view'] }}@endif">
    <nav class="navbar navbar-light bg-faded m-b-1">
        <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
            &#9776;
        </button>
        <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
            <a class="navbar-brand" href="{{ url('/admin') }}">Administrator Panel</a>
            @if (Auth::guard()->check())
                <div class="dropdown pull-xs-right">
                    <button class="btn btn-primary-outline dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <button class="dropdown-item" type="button">
                            <span class="fa fa-user"></span>
                            Profile
                        </button>
                        <button class="dropdown-item" type="button">
                            <span class="fa fa-gear"></span>
                            Settings
                        </button>
                        <div class="dropdown-divider"></div>
                        <a href="{{ url('/admin/logout') }}" class="dropdown-item">
                            <span class="fa fa-sign-out"></span>
                            Logout
                        </a>
                    </div>
                </div>
            @endif
            <div class="dropdown pull-xs-right m-r-1">
                <button class="btn btn-primary-outline dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{config('app.locale')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="/lang/pt-BR" class="dropdown-item">
                        pt-BR
                    </a>
                    <a href="/lang/en-US" class="dropdown-item">
                        en-US
                    </a>
                    <a href="/lang/es-ES" class="dropdown-item">
                        es-ES
                    </a>
                </div>
            </div>
        </div>
    </nav>

    @if (Auth::guard()->check())
        <aside class="-sidebar col-xs-12 col-md-4 col-lg-3 col-xl-2 p-r-0 hidden-sm-down">
            @include('admin.partials.sidebar')
        </aside>
        <div class="col-xs-12 col-md-8 col-lg-9 col-xl-10">
            @include('admin.partials.breadcrumb')
            @yield('content')
        </div>
    @else
        @yield('content')
    @endif

    @include('admin.partials.templates')
    <!-- JavaScripts -->
    <script src="/backend/js/app.js"></script>
</body>
</html>
