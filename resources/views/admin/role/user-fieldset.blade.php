<fieldset>
    <legend>@lang('model.user.role')</legend>
    <div class="form-group">
        <label class="text-help" for="">Role</label>
        <select name="role" id="role" class="c-select form-control">
            <option value="">– Select a role –</option>
            <option value="administrator">Administrator</option>
            <option value="partner">Partner</option>
        </select>
    </div>
</fieldset>