<fieldset id="customer">
    <legend>@lang('model.customer.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.customer.birthday')</label>
            {!! Form::text('customer[birthday]', null, ['id' => 'customer-birthday', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.customer.document')</label>
            {!! Form::text('customer[document]', null, ['id' => 'customer-document', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.customer.telephone')</label>
            {!! Form::text('customer[telephone]', null, ['id' => 'customer-telephone', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.customer.card_brand')</label>
            {!! Form::text('customer[card_brand]', null, ['id' => 'customer-card_brand', 'class' => 'form-control', 'readonly']) !!}
        </div>
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.customer.card_last_four')</label>
            {!! Form::text('customer[card_last_four]', null, ['id' => 'customer-card_last_four', 'class' => 'form-control', 'readonly']) !!}
            {!! Form::hidden('customer[address_id]', null, ['id' => 'customer-address_id']) !!}
        </div>
    </div>
</fieldset>
