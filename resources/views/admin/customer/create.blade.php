@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/customer/create',
        'method' => 'post',
        'id' => 'form-customer'
    )) !!}

        @include('admin/customer/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection