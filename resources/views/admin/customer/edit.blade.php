@extends('admin.layouts.app')

@section('content')
    {!! Form::model($customer, array(
        'method' => 'post',
        'id' => 'form-customer',
        'action' => ['Admin\CustomerController@update', $customer->id]
    )) !!}

        @include('admin.customer.form', ['submitButton' => 'Update', 'customer' => $customer])

    {!! Form::close() !!}
@endsection