@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/post/create',
        'id' => 'form-post',
        'method' => 'post'
    )) !!}

        @include('admin/post/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
