@extends('admin.layouts.app')

@section('content')
    {!! Form::model($post, array(
        'method' => 'post',
        'action' => ['Admin\PostController@update', $post->id],
        'id' => 'form-post'
    )) !!}

        @include('admin.post.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
