<fieldset id="package">
    <legend>@lang('model.post.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="post-title">@lang('model.post.title')</label>
            {!! Form::text('post[title]', null, ['id' => 'post-title', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="post-slug">@lang('model.post.slug')</label>
            {!! Form::text('post[slug]', null, ['id' => 'post-slug', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="product-content">@lang('model.product.content')</label>
        {!! Form::hidden('post[content]', null, ['id' => 'post-content', 'class' => 'form-control']) !!}
        <div class="-dante">
            <div id="page-editor" class="-content">
                @if(old('post')['content'])
                    {!!old('post')['content']!!}
                @else
                    {!!@$post[content]!!}
                @endif
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="post-excerpt">@lang('model.post.excerpt')</label>
        {!! Form::textarea('post[excerpt]', null, ['id' => 'post-excerpt', 'class' => 'form-control', 'rows' => '5']) !!}
    </div>
</fieldset>
