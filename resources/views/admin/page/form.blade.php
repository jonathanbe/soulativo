<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.page.fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
    @include('admin.gallery.thumbnail', [
        'id' => 'page-thumbnail_modal',
        'media' => @$page->thumbnail,
        'name' => 'page[thumbnail_id]'
    ])
</aside>
