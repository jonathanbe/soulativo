@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/page/create',
        'id' => 'form-page',
        'method' => 'post'
    )) !!}

        @include('admin/page/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection
