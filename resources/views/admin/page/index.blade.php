@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">@lang('model.page.plural') <a href="{{ url('/admin/page/create') }}" class="btn btn-primary">Add new @lang('model.page.singular')</a></a>
            @include('admin.search.form', ['action' => '/page/search'])
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($pages)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_pages)}})</a>
            </li>
        </ul>

        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/page/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.page.title')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pages as $page)
                        <tr>
                            <td>
                                <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$page->id}}">
                                <a href="{{ url('admin/page/edit/'.$page->id) }}">{{$page->title}}</a></td>
                            <td><small>{{ isset($page->created_at) ? $page->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/page" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
            <input type="hidden" name="trash_delete" id="trash_delete" value="1">
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/page/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/page/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_pages) === 0) hidden-xs-up @endif">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> @lang('model.page.title')
                            </th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($deleted_pages as $deleted_page)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$deleted_page->id}}">
                                    <a href="{{ url('admin/page/edit/'.$deleted_page->id) }}">{{$deleted_page->title}}</a></td>
                                <td><small>{{ isset($deleted_page->created_at) ? $deleted_page->created_at->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if (count($deleted_pages) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/page/templates/list')
@endsection
