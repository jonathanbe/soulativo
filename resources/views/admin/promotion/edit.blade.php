@extends('admin.layouts.app')

@section('content')
    {!! Form::model($promotion, array(
        'method' => 'post',
        'action' => ['Admin\PromotionController@update', $promotion->id],
        'id' => 'form-promotion'
    )) !!}

        @include('admin.promotion.form', ['submitButton' => 'Update'])

    {!! Form::close() !!}
@endsection
