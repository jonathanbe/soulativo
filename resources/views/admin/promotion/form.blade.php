<div class="col-xs-12 col-xl-8">
    @include('admin.partials.flash')
    @include('admin.promotion.fieldset')
</div>

<aside class="col-xs-12 col-xl-4">
    @include('admin.partials.form.publish-fieldset')
</aside>
