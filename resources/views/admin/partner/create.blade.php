@extends('admin.layouts.app')

@section('content')
    {!! Form::open(array(
        'url' => 'admin/partner/create',
        'id' => 'form-partner'
    )) !!}

        @include('admin/partner/form', ['submitButton' => 'Create'])

    {!! Form::close() !!}
@endsection