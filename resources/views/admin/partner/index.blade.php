@extends('admin.layouts.app')

@section('content')
    <div class="col-xs-12">
        <nav class="navbar navbar-light bg-faded m-b-1" style="background-color: #e3f2fd;">
            <a class="navbar-brand">Partners <a href="{{ url('/admin/partner/create') }}" class="btn btn-primary">Add new Partner</a></a>
            @include('admin.search.form', ['action' => '/partner/search'])
        </nav>

        <ul class="nav nav-tabs" id="nav-tab">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="all">All ({{count($partners)}})</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tab" aria-controls="trasg">Trash ({{count($deleted_users)}})</a>
            </li>
        </ul>

        <div class="tab-content">
            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'all', 'class' => 'tab-pane active')) !!}
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/partner/destroy" data-target="#trash" class="dropdown-item">Move to Trash</button>
                        </div>
                    </div>
                </header>
                <table class="table table-striped">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> Name
                            </th>
                            <th>E-mail</th>
                            <th>Last login</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($partners as $partner)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$partner->id}}">
                                    <a href="{{ url('admin/partner/edit/'.$partner->id) }}">{{$partner->name}}</a></td>
                                <td>{{$partner->email}}</td>
                                <td><small>{{ isset($partner->last_login) ? $partner->last_login->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <nav>
                    <ul class="pager">
                        <li><a href="/partner" class="paginate" data-page="1" data-limit="{{$perPage}}">Load more</a></li>
                    </ul>
                </nav>
            {!! Form::close() !!}

            {!! Form::open(array('url' => '', 'method' => 'POST', 'id' => 'trash', 'class' => 'tab-pane')) !!}
                <input type="hidden" name="trash_delete" id="trash_delete" value="1">
                <header class="m-t-1 m-b-1">
                    <div class="btn-group m-r-1">
                        <button type="button" class="btn btn-primary-outline btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            – Actions in mass –
                        </button>
                        <div class="dropdown-menu">
                            <button data-action="/admin/user/destroy" class="dropdown-item">Delete permanently</button>
                            <button data-action="/admin/user/restore" data-target="#all" class="dropdown-item">Restore</button>
                        </div>
                    </div>
                </header>

                <table class="table table-striped @if (count($deleted_users) === 0) hidden-xs-up @endif ">
                    <thead class="">
                        <tr>
                            <th>
                                <input type="checkbox" class="m-r-1" value=""> Name
                            </th>
                            <th>E-mail</th>
                            <th>Last login</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($deleted_users as $deleted_user)
                            <tr>
                                <td>
                                    <input type="checkbox" name="checkbox[]" class="m-r-1" value="{{$deleted_user->id}}">
                                    <a href="{{ url('admin/user/edit/'.$deleted_user->id) }}">{{$deleted_user->name}}</a></td>
                                <td>{{$deleted_user->email}}</td>
                                <td><small>{{ isset($deleted_user->last_login) ? $deleted_user->last_login->format(trans('model.dateformat.full')) : ' - ' }}</small></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @if (count($deleted_users) === 0)
                    <div class="alert alert-info" id="empty-trash" role="alert">
                        <strong>Empty trash!</strong> There' nothing inside the trash.
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>

    <!-- Inclues template for pagination -->
    @include('admin/partner/templates/list')
@endsection
