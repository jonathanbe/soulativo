<fieldset id="partner">
    <legend>@lang('model.partner.singular')</legend>
    <div class="row">
        <div class="form-group col-xs-12 col-xl-6">
            <label class="text-help" for="">@lang('model.partner.document')</label>
            {!! Form::text('partner[document]', null, ['id' => 'partner-document', 'class' => 'form-control']) !!}
        </div>
    </div>
</fieldset>
