@extends('admin.layouts.app')

@section('content')
{!! Form::open(array('url' => '/admin/login', 'class' => 'col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3 p-t-3')) !!}
    {!! csrf_field() !!}

    <div class="form-group row">
        {{ Form::label('email', 'E-mail address', ['class' => 'col-sm-4 form-control-label text-xs-right font-weight-bold']) }}
        <div class="col-sm-8">
            {{ Form::email('email', null, ['class' => 'form-control input-md', 'placeholder' => 'example@gmail.com']) }}

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
        {{ Form::label('password', 'Password', ['class' => 'col-sm-4 form-control-label text-xs-right font-weight-bold']) }}
        <div class="col-sm-8">
            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => '••••••••']) }}

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="col-sm-offset-4 col-sm-8">
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i>Login
            </button>

            <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Password?</a>
        </div>
    </div>
{!! Form::close() !!}
@endsection
