@extends('layouts.app', ['submenu' => 'not-fixed'])

@section('content')

@if(@$customer)
    {!! Form::model($customer, array(
        'method' => 'post',
        'class' => 'checkout -form-default',
        'action' => ['CustomerController@store']
    )) !!}
@else
    {!! Form::open(array('url' => '/checkout/customer', 'class' => 'checkout -form-default')) !!}
@endif
    <header class="-steps">
        <h3>@lang('front.checkout.customer.step')</h3>
        <h1>@lang('front.checkout.customer.title')</h1>
        <p>@lang('front.checkout.customer.description')</p>
        <a href="/cart">« @lang('front.checkout.customer.back')</a>
    </header>
    <div class="container">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            @include('partials.alerts')
        </div>
        <div class="customer col-xs-12 col-md-6 col-md-offset-3">
            @include('address.fieldset')
            @include('customer.fieldset')
        </div>

        <footer class="col-xs-12 col-md-6 col-md-offset-3">
            <input type="submit" name="name" class="-block" value="@lang('front.checkout.submit')">
        </footer>
    </div>
{!! Form::close() !!}

@endsection
