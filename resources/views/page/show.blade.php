@extends('layouts.app')

@section('content')
    <header class="-page-head" style="background-image: url(/images/delete/bg-top-02.jpg)">
        <div class="container">
            <div class="wrapper">
                <h1>{{$page->title}}</h1>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="page -read col-xs-12 col-md-8">
            <div class="-content">
                
                <p>{{$page->excerpt}}</p>
                @if(!is_null($page->thumbnail))
                <figure>
                    <img src="/images/{{$page->thumbnail->filename}}" alt="{{$page->thumbnail->title}}">
                    
                    <figcaption>{{$page->thumbnail->caption}}</figcaption>
                </figure>
                @endif
                
                {!! $page->content !!}
            </div>
        </div>
        <aside class="-sidebar col-xs-12 col-md-3 col-md-offset-1">
            @include('page.aside')
            @include('newsletter.aside')
        </aside>
    </div>
@endsection
