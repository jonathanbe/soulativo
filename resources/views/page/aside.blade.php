<nav>
    @foreach(\App\Models\Page::all() as $page)
        <a href="/{{$page->slug}}" @if(@$slug == $page->slug) class="-active" @endif >{{$page->title}}</a>
    @endforeach
    <a href="/blog">Blog</a>
</nav>
