<nav class="social @if(!empty($class)) {{$class}} @endif">
    <a href="https://www.facebook.com/soulativo/" target="_blank" class="icon icon-icon-facebook"></a>
    <a href="https://www.instagram.com/soulativo/" target="_blank" class="icon icon-instagram19"></a>
    <a href="https://twitter.com/soulativo" target="_blank" class="icon icon-icon-twitter"></a>
</nav>
