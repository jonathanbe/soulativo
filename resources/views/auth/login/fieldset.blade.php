<fieldset>
    <div class="form-group">
        {{ Form::label('login-email', 'Email') }}
        {{ Form::email('email', null, ['class' => 'form-control -dashed', 'placeholder' => 'example@gmail.com', 'id' => 'login-email']) }}
    </div>
    <div class="form-group">
        {{ Form::label('login-password', 'Senha') }}
        {{ Form::password('password', ['class' => 'form-control -dashed', 'id' => 'login-password']) }}
    </div>
    <div class="form-group">
        <input type="hidden" name="redirectUrl" value="{{@$redirectUrl}}">
        <a href="#" class="lost-password">Esqueci a senha</a>
        <input type="submit" value="Entrar »">
    </div>
</fieldset>
