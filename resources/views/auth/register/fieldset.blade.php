<fieldset>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('register-first_name', 'Nome') }}
            {{ Form::text('first_name', null, ['class' => 'form-control -dashed', 'id' => 'register-first_name']) }}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('register-last_name', 'Sobrenome') }}
            {{ Form::text('last_name', null, ['class' => 'form-control -dashed', 'id' => 'register-last_name']) }}
        </div>
    </div>
    <div class="form-group">
        {{ Form::label('register-email', 'Email') }}
        {{ Form::email('email', null, ['class' => 'form-control -dashed', 'placeholder' => 'example@gmail.com', 'id' => 'register-email']) }}
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('register-password', 'Senha', ['class' => 'text-help']) }}
            {{ Form::password('password', ['class' => 'form-control -dashed', 'id' => 'register-password']) }}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            {{ Form::label('register-password_confirmation', 'Confirmation') }}
            {{ Form::password('password_confirmation', ['class' => 'form-control -dashed', 'id' => 'register-password_confirmation']) }}
        </div>
    </div>
    <div class="form-group">
        <input type="hidden" name="redirectUrl" value="{{@$redirectUrl}}">
        <input type="submit" value="Cadastre-se »">
    </div>
</fieldset>
