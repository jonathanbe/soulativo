<fieldset>
    <h2>@lang('front.profile.address')</h2>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.country.singular')</label>
            <select name="address[country_id]" class="c-select -dashed">
                @foreach($countries as $id => $item)
                    @if($id == '')
                        <option value="">{{$item}}</option>
                    @else
                        <option value="{{$id}}" @if(!is_null(@$user->customer->address) and @$user->customer->address->country_id == $id) selected="selected" @endif data-calling-code="{{$item['calling_code']}}">{{$item['name']}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.address.state')</label>
            <!-- Text for outsiders -->
            {!! Form::text('address[state]', null, ['id' => 'address-state', 'class' => 'form-control -dashed', ((@$user->customer->address->country->iso_3166_2 == 'BR') ? 'disabled' : 'enabled' )]) !!}

            <!-- Select for brazilians -->
            {!! Form::select('address[state]', [ "AC"=>"Acre", "AL"=>"Alagoas", "AM"=>"Amazonas", "AP"=>"Amapá","BA"=>"Bahia","CE"=>"Ceará","DF"=>"Distrito Federal","ES"=>"Espírito Santo","GO"=>"Goiás","MA"=>"Maranhão","MT"=>"Mato Grosso","MS"=>"Mato Grosso do Sul","MG"=>"Minas Gerais","PA"=>"Pará","PB"=>"Paraíba","PR"=>"Paraná","PE"=>"Pernambuco","PI"=>"Piauí","RJ"=>"Rio de Janeiro","RN"=>"Rio Grande do Norte","RO"=>"Rondônia","RS"=>"Rio Grande do Sul","RR"=>"Roraima","SC"=>"Santa Catarina","SE"=>"Sergipe","SP"=>"São Paulo","TO"=>"Tocantins"],
                null, ['id' => 'address-state-brazil', 'class' => 'form-control -dashed c-select', ((@$user->customer->address->country->iso_3166_2 != 'BR') ? 'disabled' : 'enabled' )]) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.address.city')</label>
            {!! Form::text('address[city]', null, ['id' => 'address-city', 'class' => 'form-control -dashed']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.address.zipcode')</label>
            {!! Form::text('address[zipcode]', null, ['id' => 'address-zipcode', 'class' => 'form-control -dashed']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-9">
            <label class="text-help" for="">@lang('model.address.street')</label>
            {!! Form::text('address[street]', null, ['id' => 'address-street', 'class' => 'form-control -dashed']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-3">
            <label class="text-help" for="">@lang('model.address.number')</label>
            {!! Form::text('address[number]', null, ['id' => 'address-number', 'class' => 'form-control -dashed']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.address.neighbourhood')</label>
            {!! Form::text('address[neighbourhood]', null, ['id' => 'address-neighbourhood', 'class' => 'form-control -dashed']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            <label class="text-help" for="">@lang('model.address.complement')</label>
            {!! Form::text('address[complement]', null, ['id' => 'address-complement', 'class' => 'form-control -dashed']) !!}
        </div>
    </div>
</fieldset>
