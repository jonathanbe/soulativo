<article>
    <p>@lang('front.order.list.date') {{$order->created_at}}</p>
    <div class="wrapper">
        <header>
            <h2>@lang('model.order.singular') #{{$order->id}}</h2>
        </header>
        @foreach($order->bookings as $booking)
            <article>
                <header>
                    <h3><a href="/package/{{$booking->package->product->slug}}">{{$booking->package->product->name}}</a></h3>
                    <div class="status">
                        @if($booking->status == 1)
                            <span class="text">
                                Status
                                <strong>@lang('front.order.status.pending')</strong>
                            </span>
                            <span class="square -pending"></span> <!-- -approved | -pending | -rejected -->
                        @elseif($booking->status == 2)
                            <span class="text">
                                Status
                                <strong>@lang('front.order.status.approved')</strong>
                            </span>
                            <span class="square -approved"></span> <!-- -approved | -pending | -rejected -->
                        @elseif($booking->status == 3)
                            <span class="text">
                                Status
                                <strong>@lang('front.order.status.rejected')</strong>
                            </span>
                            <span class="square -rejected"></span> <!-- -approved | -pending | -rejected -->
                        @endif
                    </div>
                </header>
                <h4>@lang('model.booking.reference') #{{$booking->id}}</h4>
                <ul>
                    <li>{{$booking->date}} (@lang('front.order.pickup') {{$booking->departure_hour}})</li>
                    <li>{{$booking->adults}} @lang('front.booking.adult.plural') ({{config('app.currency.prefix') . $booking->getDiscountedPrice()}} @lang('front.booking.each'))</li>
                    @if($booking->childs > 0)
                        <li>{{$booking->childs}} @lang('front.booking.child.plural') ({{config('app.currency.prefix') . $booking->getChildPrice()}} @lang('front.booking.each'))</li>
                    @endif
                </ul>
                <span class="price">{{config('app.currency.prefix')}}{{ $booking->getTotal() }}</span>
            </article>
        @endforeach
        <footer>
            <div class="total">
                <span>Total</span>
                <span>{{config('app.currency.prefix') . ($order->getTotal())}}</span>
            </div>
        </footer>
    </div>
</article>
