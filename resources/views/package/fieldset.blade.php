<fieldset>
    <div class="form-group">
        <label class="text-help" for="booking-date">
          @lang('front.booking.when')
          <span class="icon icon-icon-magnifier"></span>
        </label>
        <div class="row">
            <div class="col-xs-12">
                <input type="text" name="booking[date]" id="booking-date" class="form-control -dashed">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('front.booking.many')</label>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <select name="booking[adults]" class="c-select -dashed">
                    @for ($i = 1; $i < 10; $i++)
                        <option value="{{$i}}">
                             @if($package->unity == 1)
                                {{$i}} @if($i > 1)@lang('front.booking.reservation.plural') @else @lang('front.booking.reservation.singular') @endif
                            @else
                                {{$i}} @if($i > 1)@lang('front.booking.adult.plural') @else @lang('front.booking.adult.singular') @endif
                            @endif
                            ({{config('app.currency.prefix') . $package->product->getDiscountedPrice() * $i}})
                        </option>
                    @endfor
                </select>
            </div>
            <div class="col-xs-12 col-lg-6 {{ ($package->adults_only) ? 'hidden' : '' }}">
                <select name="booking[childs]" class="c-select -dashed">
                    <option value="0">@lang('front.booking.child.empty')</option>
                    @for ($i = 1; $i < 10; $i++)
                        <option value="{{$i}}">
                            {{$i}} @if($i > 1) @lang('front.booking.child.plural') @else @lang('front.booking.child.singular') @endif
                            ({{config('app.currency.prefix') . $package->product->getChildPrice() * $i}})</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    <div class="form-group total">
        <label for="text-help" for="">Total</label>
        <span>{{config('app.currency.prefix')}}<span class="text-total">{{$package->product->getDiscountedPrice()}}</span> <span class="text-lowercase">@lang('front.package.parcel')</span></span>
    </div>
</fieldset>
