@extends('layouts.app')

@section('content')

    <header class="-page-head" style="background-image: url('/images/delete/bg-top-01.jpg')">
        <div class="container">
            <div class="wrapper">
                <h1>{{$category->name}}</h1>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="package -list">
            @if(count($packages) > 0)
                <?php $cont = 1; ?>
                @foreach($packages as $package)
                    @if($cont == 3)
                        @include('package.loop', ['size' => 'big'])
                        <?php $cont = 1; ?>
                    @elseif($package == end($packages) && $cont != 1)
                        @include('package.loop', ['size' => 'big'])
                    @else
                        @include('package.loop', ['size' => 'medium'])
                        <?php $cont++; ?>
                    @endif
                @endforeach
            @else
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        <p>Não há produtos para a busca por {{$search}}</p>
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection
