<div class="price">
    @if($package->product->getPrice() != $package->product->getDiscountedPrice())
        <span class="old">{{config('app.currency.prefix') . $package->product->getPrice()}}</span>
    @endif
    <span class="current">{{config('app.currency.prefix') . $package->product->getDiscountedPrice()}}</span>
    <span class="info">
        <span>@lang('front.package.parcel')</span>
        @if($package->product->getPrice() != $package->product->getDiscountedPrice())
            <span class="-highlight">@lang('front.product.discount', ['percent' => $package->product->getDiscountedPercent()])</span>
        @endif
    </span>
</div>
