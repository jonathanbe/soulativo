@extends('layouts.app')

@section('content')
    <header class="-page-head" style="background-image: url(/images/delete/bg-top-02.jpg)">
        <div class="container">
            <div class="wrapper">
                <h1>@lang('front.contact.title')</h1>
            </div>
        </div>
    </header>
    <div class="contact container">
        <div class="col-xs-12 col-md-8">
            <div class="-content">
                <p>{{$page->excerpt}}</p>
            </div>

            @include('admin.partials.flash')
            {!! Form::open(array('url' => '/contact', 'class' => '-form-default')) !!}
                <h3>@lang('front.contact.form.title')</h3>
                <fieldset>
                    <div class="row">
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="">@lang('model.user.first_name')</label>
                            <input type="text" name="name" class="form-control -dashed">
                        </div>
                        <div class="form-group col-xs-12 col-md-6">
                            <label for="">@lang('model.user.email')</label>
                            <input type="email" name="email" class="form-control -dashed">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">@lang('front.contact.form.subject')</label>
                        <input type="text" name="subject" class="form-control -dashed">
                    </div>
                    <div class="form-group">
                        <label for="">@lang('front.contact.form.message')</label>
                        <textarea name="message" id="message" class="form-control -rounded" rows="7"></textarea>
                    </div>
                </fieldset>
                <footer>
                    <input type="submit" name="send" value="@lang('front.contact.form.submit') »">
                </footer>
            {!! Form::close() !!}
        </div>
        <aside class="-sidebar col-xs-12 col-md-3 col-md-offset-1">
            <h4>@lang('front.contact.aside.title')</h4>
            <nav class="social">
                <a href="https://www.facebook.com/soulativo/" target="_blank">Facebook</a>
                <a href="https://www.instagram.com/soulativo/" target="_blank">Instagram</a>
                <a href="mailto:soulativo@soulativo.com" target="_blank">Email</a>
            </nav>
            <nav class="phones">
                <span>+55 21 99657-3436 (WhatsApp Português)</span>
                <span>+55 21 98696-2020 (WhatsApp English)</span>
            </nav>
            <a href="/faq" class="faq">@lang('front.contact.aside.faq')</a>
        </aside>
    </div>
@endsection
