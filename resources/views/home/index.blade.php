@extends('layouts.app')

@section('content')
    @include('package.featured')

    <div class="container">
        <div class="package -list">
            @include('package.loop', ['size' => 'medium','package' => $package1])
            @include('package.loop', ['size' => 'medium','package' => $package2])
            @include('package.loop', ['size' => 'big','package' => $package3])
        </div>
        <div class="blog -list col-xs-12 col-md-8 col-lg-8">
            <h3>@lang('front.blog.latest') <a href="/blog">@lang('front.blog.more') »</a></h3>
            <div class="row">
                @foreach($recentPosts as $recent)
                    @include('blog.tiny')
                @endforeach
            </div>
        </div>
        <aside class="-sidebar col-xs-12 col-md-4 col-lg-3 col-lg-offset-1 m-t-0">
            @include('partials.instagram')
            @include('partials.tripadvisor')
        </aside>
    </div>
@endsection
