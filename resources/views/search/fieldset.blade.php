<fieldset>
    <div class="form-group">
        <label class="text-help" for="">@lang('front.booking.when')</label>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <input type="text" name="search[from]" placeholder="@lang('front.booking.from')" class="form-control -rounded">
            </div>
            <div class="col-xs-12 col-lg-6">
                <input type="text" name="search[to]" placeholder="@lang('front.booking.to')" class="form-control -rounded">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('front.booking.many')</label>
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <select name="search[adults]" class="c-select -dashed">
                    <option value="1">1 @lang('front.booking.adult.singular')</option>
                    <option value="2">2 @lang('front.booking.adult.plural')</option>
                    <option value="3">3 @lang('front.booking.adult.plural')</option>
                    <option value="4">4 @lang('front.booking.adult.plural')</option>
                    <option value="5">5 @lang('front.booking.adult.plural')</option>
                    <option value="6">6 @lang('front.booking.adult.plural')</option>
                    <option value="7">7 @lang('front.booking.adult.plural')</option>
                    <option value="8">8 @lang('front.booking.adult.plural')</option>
                    <option value="8">8 @lang('front.booking.adult.plural')</option>
                    <option value="9">9 @lang('front.booking.adult.plural')</option>
                </select>
            </div>
            <div class="col-xs-12 col-lg-6">
                <select name="search[childs]" class="c-select -dashed">
                    <option value="0">@lang('front.booking.child.empty')</option>
                    <option value="1">1 @lang('front.booking.child.singular')</option>
                    <option value="2">2 @lang('front.booking.child.plural')</option>
                    <option value="3">3 @lang('front.booking.child.plural')</option>
                    <option value="4">4 @lang('front.booking.child.plural')</option>
                    <option value="5">5 @lang('front.booking.child.plural')</option>
                    <option value="6">6 @lang('front.booking.child.plural')</option>
                    <option value="7">7 @lang('front.booking.child.plural')</option>
                    <option value="8">8 @lang('front.booking.child.plural')</option>
                    <option value="8">8 @lang('front.booking.child.plural')</option>
                    <option value="9">9 @lang('front.booking.child.plural')</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-help" for="">@lang('front.search.experience.title')</label>
        <select name="search[experience]" class="c-select -dashed">
            <option value="0">@lang('front.search.experience.beginner')</option>
            <option value="1">@lang('front.search.experience.radical')</option>
        </select>
    </div>
    <footer>
        <input type="submit" value="@lang('front.search.submit') »">
    </footer>
</fieldset>
