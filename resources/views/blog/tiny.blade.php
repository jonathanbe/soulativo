<article class="-tiny @if(@$class) {{$class}} @else col-xs-12 col-md-6 @endif">
    <figure>
        <a href="/post/{{$recent->slug}}">
            @if(!empty($recent->thumbnail_id))
                <img src="/images/{{$recent->thumbnail->filename}}?w=340&h=150&fit=crop-340-150">
            @endif
        </a>
    </figure>
    <p>{{$recent->title}}</p>
</article>
