<article class="-loop">
    <div class="row">
        <figure class="col-xs-4 col-md-3">
            @if(!empty($post->thumbnail_id))
                <img src="/images/{{$post->thumbnail->filename}}?w=150&h=150&fit=crop-50-50" alt="">
            @endif
        </figure>
        <div class="col-xs-12 col-md-9">
            <span class="cat">
                @foreach($post->categories as $cat)
                    <a href="/posts/{{$cat->slug}}">{{$cat->name}}</a>
                @endforeach
            </span>
            <h3><a href="/post/{{$post->slug}}">{{$post->title}}</a></h3>
            <time>{{ isset($post->created_at) ? $post->created_at->format(trans('model.dateformat.full')) : ' - ' }}</time>
            <p>{{$post->excerpt}}</p>
        </div>
    </div>
</article>
