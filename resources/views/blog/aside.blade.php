<nav>
    @foreach($categories->getDescendants() as $category)
        <a href="/posts/{{$category->slug}}" @if(isset($slug) and $category->slug == $slug) class="-active" @endif>{{$category->name}}</a> <!-- FIXME: Fix -active color to $cool-grey -->
    @endforeach
</nav>
