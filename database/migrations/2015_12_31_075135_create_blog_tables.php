<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('posts', function ($table) {
			$table->increments('id');
            $table->integer('thumbnail_id')->nullable()->unsigned();

			$table->timestamps();
            $table->softDeletes();

            $table->engine = 'InnoDB';
            $table->foreign('thumbnail_id')->references('id')->on('medias')->onDelete('set null');
		});

        Schema::create('category_post', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->engine = 'InnoDB';
            $table->primary(['category_id', 'post_id']);
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        /*
         * Translations
         * */
        Schema::create('post_translations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->text('excerpt');

            $table->engine = 'InnoDB';
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('locale')->references('code')->on('locales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
        Schema::drop('post_translations');
    }
}
