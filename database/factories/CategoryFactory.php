<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Category;
use App\Models\CategoryTranslation;

$factory->define(Category::class, function (Faker\Generator $faker, $args) {

    if(!empty($args['category_id'])){
        $category = Category::find($args['category_id']);
    }else{
        $category = new Category();
    }

    if(!empty($args['parent_id'])){
        $category->parent_id = $args['parent_id'];
    }
    $category->save();

    $category_id = $category->id;

    $translation = new CategoryTranslation();
    $translation->name = $args['name'];
    $translation->slug = $args['slug'];
    $translation->locale = $args['locale'];
    $translation->category_id = $category_id;
    $translation->save();

    $category['teste'] = 'teste';

    return [$category];
});
