<?php

use Illuminate\Database\Seeder;
use App\Models\PostTranslation;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = factory(\App\Models\Post::class, 6)
           ->create()
           ->each(function($u) {
                $u->save();
                $u->categories()->sync([18]);

                $translation = PostTranslation::where('post_id', $u->id)->first();
                $translation->locale = 'pt-BR';
                $translation->save();
            });
    }
}
