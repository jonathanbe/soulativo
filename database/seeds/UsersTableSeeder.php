<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Giseudo
        DB::table('users')->insert([
            'first_name' => 'Giseudo',
            'last_name' => 'Oliveira',
            'email' => 'giseudo@gmail.com',
            'password' => bcrypt('32620393'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);

        // Jonathan
        DB::table('users')->insert([
            'first_name' => 'Jonathan',
            'last_name' => 'Beltrão',
            'email' => 'jonathanbeltrao@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 1
        ]);

        // Rogério
        DB::table('users')->insert([
            'first_name' => 'Rogério',
            'last_name' => 'Melo',
            'email' => 'melo@soulativo.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 3,
            'role_id' => 1
        ]);

        // Anna
        DB::table('users')->insert([
            'first_name' => 'Anna',
            'last_name' => 'Mello',
            'email' => 'annamelofoto@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 4,
            'role_id' => 1
        ]);

        // Bruno
        DB::table('users')->insert([
            'first_name' => 'Bruno',
            'last_name' => 'Oliveira',
            'email' => 'bruno@ilustrebob.com',
            'password' => bcrypt('123123'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 5,
            'role_id' => 1
        ]);

        // Luciana
        DB::table('users')->insert([
            'first_name' => 'Luciana',
            'last_name' => 'Caserta',
            'email' => 'lucianacaserta@soulativo.com.br',
            'password' => bcrypt('123123'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => 6,
            'role_id' => 1
        ]);
    }
}
