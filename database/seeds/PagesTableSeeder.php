<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use App\Models\PageTranslation;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = new Page();
        $page->title = 'Empresa';
        $page->slug = 'empresa';
        $page->excerpt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam varius nisl non tempor feugiat. Nulla ligula ex, elementum sollicitudin nibh pharetra, porttitor consequat libero.';
        $page->content = '<h2>Lorem upsum sit amet</h2><p>Praesent sit amet arcu eget diam pharetra tristique sed eget enim. Nam molestie elementum lorem, pulvinar lacinia eros eleifend at. Suspendisse nec ligula sit amet massa aliquet tempor quis sed nibh. Aenean lobortis tellus facilisis ex aliquam scelerisque.</p>';
        $page->save();

        $translation = PageTranslation::where('page_id', $page->id)->first();
        $translation->locale = 'pt-BR';
        $translation->save();
    }
}
