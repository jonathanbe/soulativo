<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'slug' => 'administrator',
            'name' => 'Administrator'
        ]);

        DB::table('roles')->insert([
            'slug' => 'partner',
            'name' => 'Partner'
        ]);

        DB::table('roles')->insert([
            'slug' => 'customer',
            'name' => 'Customer'
        ]);
    }
}
