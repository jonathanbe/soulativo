<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Partner extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'document'
    ];
    
    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'partner.document' => 'required|min:7'
    ];

    /**
     * Get the customer's user.
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }

    /**
     * Get the customer's address.
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }
    
    /**
    * Get the user's full name.
    *
    * @return string
    */
    public static function getValidation()
    {
            return static::$validation;
    }
}
