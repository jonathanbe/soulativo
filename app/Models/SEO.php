<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SEO extends Model
{
    protected $table = 'seo';

    protected $fillable = ['title', 'keywords', 'description'];
    protected $dates = ['created_at', 'updated_at'];
    public static $root_slug = 'seo';

    public static function sitemap () {

    $xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach(Package::all() as $pkg){
            $xml .= '<url>
                        <loc>https://soulativo.com.br/package/' . $pkg->product->slug . '</loc>
                        <changefreq>weekly</changefreq>
                     </url>';
        }
        foreach(Post::all() as $post){
            $xml .= '<url>
                        <loc>https://soulativo.com.br/post/' . $post->slug . '</loc>
                        <changefreq>weekly</changefreq>
                     </url>';
        }
        foreach(Category::all() as $category){
            $xml .= '<url>
                        <loc>https://soulativo.com.br/posts/' . $category->slug . '</loc>
                        <changefreq>weekly</changefreq>
                     </url>';
        }
        foreach(Category::all() as $category){
            $xml .= '<url>
                        <loc>https://soulativo.com.br/packages/' . $category->slug . '</loc>
                        <changefreq>weekly</changefreq>
                     </url>';
        }
        foreach(Page::all() as $page){
            $xml .= '<url>
                        <loc>https://soulativo.com.br/page/' . $page->slug . '</loc>
                        <changefreq>weekly</changefreq>
                     </url>';
        }

        $xml .= '</urlset>';

        header("Content-type: text/xml");
        echo $xml;
    }
}
