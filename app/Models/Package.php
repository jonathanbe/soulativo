<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;
use App\Models\Category;
use App\Models\ProductTranslation;
use App\Models\Blockout;

class Package extends Model
{
    use SoftDeletes;
    use Translatable;
    use Validatable;
    use Gallery;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['destination', 'map_data', 'gallery', 'position', 'terms', 'product_id', 'adults_only', 'unity', 'order','thumbnail_id'];
    public static $root_slug = 'packages';
    public $translatedAttributes = ['terms'];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [

    ];

    /**
     * Get the customer's address.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function thumbnail()
    {
        return $this->hasOne(Media::class, 'id', 'thumbnail_id');
    }

    public function seo()
    {
        return $this->belongsTo(SEO::class);
    }

    public function blockouts()
    {
        return $this->hasMany(Blockout::class);
    }

    public static function findBySlug($slug)
    {
        $translation = ProductTranslation::where('slug', $slug)->first();
        return Package::where('product_id', $translation->product_id)->first();
    }

    public static function getAllByCategorySlug($slug)
    {
        $products = Product::getAllByCategorySlug($slug);
        $packages = [];
        foreach($products as $product){
            $packages[] = $product->package;
        }
        return $packages;
    }

    public static function setFeatured($id)
    {
        $returnUpdate = Package::where('id', '>=',1) ->update(array('featured' => '0'));
        $pk = Package::findOrFail($id);
        $pk->featured = 1;
        $pk->save();
        return json_encode([
            'error' => false
        ]);
    }

    public function setThumbnailIdAttribute($value)
    {
        $this->attributes['thumbnail_id'] = $value ?: null;
    }
}

class PackageTranslation extends Model {

    public $timestamps = false;
    protected $fillable = ['terms'];

}
