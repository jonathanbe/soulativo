<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;
use App\Models\CategoryTranslation;
use Baum;

class Category extends Baum\Node {

    use SoftDeletes;
    use Translatable;
    use Validatable;

    public $timestamps = false;
    public $translatedAttributes = ['name', 'slug'];
    protected $fillable = ['name', 'slug', 'parent_id', 'thumbnail_id'];
    protected $dates = ['created_at', 'updated_at','deleted_at'];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'category.name' => 'required|min:4',
        'category.slug' => 'required|min:4'
    ];

    public function thumbnail()
    {
        return $this->hasOne(Media::class, 'id', 'thumbnail_id');
    }

    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class);
    }

    public function posts()
    {
        return $this->belongsToMany(\App\Models\Post::class);
    }

    public function seo()
    {
        return $this->belongsTo(SEO::class);
    }

    public static function findBySlug($slug)
    {
        $translation = CategoryTranslation::where('slug', $slug)->first();
        return Category::find($translation->category_id);
    }

    public function setThumbnailIdAttribute($value)
    {
        $this->attributes['thumbnail_id'] = $value ?: null;
    }
}
