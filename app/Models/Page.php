<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;
use App\Models\PageTranslation;

class Page extends Model
{
    use SoftDeletes;
    use Translatable;
    use Validatable;

    public $translatedAttributes = ['title', 'slug', 'content', 'excerpt'];
    protected $fillable = ['title', 'slug', 'content', 'excerpt', 'thumbnail_id'];
    protected $dates = ['created_at', 'updated_at','deleted_at'];

    public static $validation = [
        'user.title' => 'required|min:4',
        'user.slug' => 'required|min:4',
        'user.content' => 'required|min:4',
        'user.excerpt' => 'required|min:4'
    ];

    public function thumbnail()
    {
        return $this->hasOne(Media::class, 'id', 'thumbnail_id');
    }

    public function seo()
    {
        return $this->belongsTo(SEO::class);
    }

    public function setThumbnailIdAttribute($value)
    {
        $this->attributes['thumbnail_id'] = $value ?: null;
    }


    public static function findBySlug($slug)
    {
        $translation = PageTranslation::where('slug', $slug)->first();
        return Page::where('id', $translation->page_id)->first();
    }
}

class PageTranslation extends Model {

    public $timestamps = false;
    protected $fillable = ['title', 'slug', 'content', 'excerpt'];

}
