<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Coupon extends Model
{
    use SoftDeletes;
    use Validatable;

    protected $fillable = ['name', 'key', 'value', 'type'];
    protected $dates = ['created_at', 'updated_at','deleted_at'];

    /**
     * The fieldset validation settings.
     *
     * @var array
     */
    public static $validation = [
        'coupon.name' => 'required|min:4',
        'coupon.value' => 'required',
        'coupon.type' => 'required'
    ];
    
    public static function generateCouponCode($length) {
	$str = "";
	$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
	$max = count($characters) - 1;
	for ($i = 0; $i < $length; $i++) {
		$rand = mt_rand(0, $max);
		$str .= $characters[$rand];
	}
	return $str;
    }

    public static function findBySlug($slug)
    {
        $translation = PostTranslation::where('slug', $slug)->first();
        return Post::where('id', $translation->post_id)->first();
    }
}
