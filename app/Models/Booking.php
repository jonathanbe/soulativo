<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use Validatable;
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'date', 'updated_at'];

    protected $fillable = [
        'date', 'childs', 'adults', 'status', 'departure_location', 'departure_hour', 'package_id'
    ];

    public static $validation = [

    ];

    public function package()
    {
        return $this->belongsTo(Package::class)->withTrashed();
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getDateAttribute()
    {
        $newDateCarbon = new Carbon($this->attributes['date']);

        return $newDateCarbon->toFormattedDateString();
    }

    public function setPackageIdAttribute($value)
    {
        if(!empty($this->attributes['package_id'])){
            if($value == $this->attributes['package_id'])
                return;

            $package = Package::find($value);
            Promotion::products([$package], 'BRL');

            $this->attributes['package_id'] = $package->id;
            $this->attributes['total'] = $package->product->getPrice('BRL');
            $this->attributes['discount'] = $this->attributes['total'] - $package->product->getDiscountedPrice('BRL');
        }else{
            $this->attributes['package_id'] = $value;
        }
    }
    
    public static function findAutoComplete($query)
    {
        $testQuery = (int)$query;
        $results = array();
        if($testQuery == 0){
            $users = User::findByName($query,3);
            foreach($users as $user){
                $orders = $user->customer->orders;
                foreach($orders as $order){
                    $bookings = $order->bookings;
                    foreach($bookings as $booking):
                        $results[] = $booking;
                    endforeach;
                }
            }
        }else{
            $results[] = Booking::find($query);
        }
        return $results;
    }

    public function setDepartureHourAttribute($value)
    {
        if(!empty($value)){
            $this->attributes['departure_hour'] = $value;
        }else{
            $this->attributes['departure_hour'] = NULL;
        }
    }

    public function getPrice($currency = null)
    {
        return round($this->total / Product::getRate($currency));
    }

    public function getDiscountedPrice($currency = null)
    {
        return round($this->getPrice($currency) - $this->getDiscount());
    }

    public function getDiscount($currency = null)
    {
        return round($this->discount / Product::getRate($currency));
    }

    public function getChildPrice($currency = null)
    {
//        dd($this->package->product->child_percent);
//        $discount = (Product::$childPercent / 100) * $this->getDiscountedPrice($currency);
        $discount = ($this->package->product->child_percent / 100) * $this->getDiscountedPrice($currency);
        return round($this->getDiscountedPrice($currency) - $discount);
    }

    public function getTotal($currency = null)
    {
        $adultSubtotal = $this->adults * $this->getDiscountedPrice($currency);
        $childSubtotal = $this->childs * $this->getChildPrice($currency);
        return $adultSubtotal + $childSubtotal;
    }
}
