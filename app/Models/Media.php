<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class Media extends Model
{
    use SoftDeletes;
    use Translatable;

    public $translatedAttributes = ['title', 'caption'];
    protected $fillable = ['title', 'caption'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $table = 'medias';

    private static $paths = [
        'image' => 'images',
        'file' => 'files'
    ];

    private static $images_ext = [
        'jpg', 'jpeg', 'png', 'gif', 'bmp', 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP'
    ];

    public static function getImageInfo($path){
        return getimagesize($path);
    }

    public static function upload($file, $newFilename){
        $path = (in_array ( $file->getClientOriginalExtension(), self::$images_ext )) ? self::$paths['image'] : self::$paths['file'];
        $dest = $path . '/' . $newFilename;
        $success = Storage::put($dest, file_get_contents($file->getRealPath()));

        if(!$success)
            return false;
        else
            return self::getImageInfo($file->getRealPath());
    }

    public static function getByJson($gallery){
        $images = [];

        if(!empty($gallery)){
            $album = json_decode($gallery);

            foreach($album->images as $image):
              $images[] = Media::find($image);
            endforeach;
        }

        return $images;
    }
}

class MediaTranslation extends Model {

    public $timestamps = false;
    protected $fillable = ['title', 'caption'];

}
