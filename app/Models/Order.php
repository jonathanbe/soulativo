<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Promotion;
use Carbon\Carbon;

class Order extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'customer_id'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function getTotal($currency = null)
    {
        // R$ 1000,00
        $total = 0;
        foreach($this->bookings as $booking){
            $total += $booking->getTotal();
        }
        return $total;
    }

    public function getDiscount()
    {
        $discountPerc = $this->coupon->value;
        $orderValue = $this->getTotal();
//        dd($orderValue);
        $discountValue = round(($discountPerc / 100.0) * $orderValue);
        
        
        return $discountValue;
        
    }

    public function getDiscountedTotal()
    {
        $discountedTotal = $this->getTotal() - $this->getDiscount();
        return $discountedTotal;
    }

    public function getCreatedAtAttribute(){
        $newDateCarbon = new Carbon($this->attributes['created_at']);

        return $newDateCarbon->toFormattedDateString();
    }

    public function getDate(){
        return new Carbon($this->attributes['created_at']);
    }
    
    public static function findAutoComplete($query)
    {
        $testQuery = (int)$query;
        $results = array();
        if($testQuery == 0){
            $users = User::findByName($query,3);
            foreach($users as $user){
                $orders = $user->customer->orders;
                foreach($orders as $order){
                        $results[] = $order;
                }
            }
        }else{
            $results[] = Order::find($query);
        }
        return $results;
    }
}
