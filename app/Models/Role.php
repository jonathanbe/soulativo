<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name'
    ];
	
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
	
    /**
     * Get a role by slug
     *
     * @param  User  $value
     * @return int
     */
	public static function getBySlug($slug){
		return Role::where('slug', $slug)->first();
	}
}
