<?php

/*
|--------------------------------------------------------------------------
| Rest API Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
use \DrewM\MailChimp\MailChimp;
use \LivePixel\MercadoPago\MP;
use App\Models\Invoice;

Route::group(['middleware' => 'web'], function () {
    Route::post('test', function (Request $request) {
      $invoice = new Invoice();
      $invoice->creditcard($request);
    });

    // Get subtotal of a Product – FIXME: We need to pass products instead packages later on
    Route::get('product/{id}/subtotal/{adults}/{childs?}', function ($id, $adults, $childs = 0) {
        $package = \App\Models\Package::where('product_id', $id)->first();

        // Apply discount
        \App\Models\Promotion::products([$package]);

        return $package->product->getSubtotal($adults, $childs);
    });

    // PayPal group
    Route::group(['prefix' => 'paypal'], function () {
        // If the card was accepted
        Route::get('success/{id}', function ($id) {
            $invoice = \App\Models\Invoice::find($id);

            // We need to pass again the same params
            $params = [
                'amount'    => $invoice->order->getTotal(),
                'currency'  => config('app.currency.name')
            ];

            $response = Omnipay::completePurchase($params)->send();

            // Saves data into invoice callback field
            $invoice->callback = json_encode($response->getData());
            $invoice->save();

            // Check for error
            if (!$response->isSuccessful()){
                Session::flash('status', $response->getMessage());
                return redirect('checkout/payment/' . $invoice->order->id);
            }else{
                return app('App\Http\Controllers\CheckoutController')->success($invoice);
            }
        });

        // If the payment was refused
        Route::get('refused/{id}', function ($id) {
            $invoice = \App\Models\Invoice::find($id);
            $invoice->delete();

            Session::flash('status', 'Payment refused');
            return redirect('checkout/payment/' . $invoice->order->id);
        });
    });

    // Mercado Pago group
    Route::group(['prefix' => 'mercadopago'], function () {
        Route::post('notification', function () {
            $mp = new MP(config('mercadopago.app_id'), config('mercadopago.app_secret'));

            if ($_GET['topic'] == 'payment') {
                return json_encode($_GET);
            } else{
                $payment_info = $mp->get("/merchant_orders/" . $_GET['id'], [
                    'access_token' => $mp->get_access_token()
                ]);

                if ($payment_info['status'] == 200) {
                    $response = $payment_info['response'];
                    $invoice = \App\Models\Invoice::find($response['external_reference']);

                    // Saves data into invoice callback field
                    $invoice->callback = json_encode($response);
                    $invoice->save();
                }

                return json_encode($payment_info);
            }
        });

        // If the card was accepted
        Route::get('success/{id}', function ($id) {

        });

        // If the payment was refused
        Route::get('refused/{id}', function ($id) {

        });
    });

    // PagSeguro group
    Route::group(['prefix' => 'pagseguro'], function () {
        Route::get('id', function (Request $request) {
            $credentials = PagSeguro::credentials()->get();

            $client = new GuzzleHttp\Client();
            $res = $client->post('https://ws.' . ((env('APP_ENV') != 'production') ? 'sandbox.' : '') .'pagseguro.uol.com.br/v2/sessions', [
              'body' => [
                'email' => $credentials->getEmail(),
                'token' => $credentials->getToken()
              ]
            ]);

            $body = $res->getBody();
            $xml = simplexml_load_string($body->getContents());

            return json_encode($xml);
        });

        // PagSeguro notifications
        Route::post('notification', [
            'as' => 'pagseguro.notification',
            function() {
                $credentials = PagSeguro::credentials()->get();

                $client = new GuzzleHttp\Client();
                $res = $client->get('https://ws.' . ((env('APP_ENV') != 'production') ? 'sandbox.' : '') .'pagseguro.uol.com.br/v3/transactions/notifications/' . $_POST['notificationCode'] .
                    '?email=' . $credentials->getEmail() .
                    '&token=' . $credentials->getToken());

                $body = $res->getBody();
                $data = simplexml_load_string($body->getContents());

                $invoice = \App\Models\Invoice::find($data->reference);
                $invoice->callback = json_encode($data);
                $invoice->save();

                return json_encode($data);
            }
        ]);

        Route::get('redirect/{id}', [
            'as' => 'pagseguro.redirect',
            function($id){
                $invoice = \App\Models\Invoice::find($id);
                return redirect('checkout/confirmation/' . $invoice->order->id);
            }
        ]);

    });

    // Blog
    Route::group(['prefix' => 'post'], function() {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $posts = \App\Models\Post::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($posts as $post){
                $post['thumbnail'] = $post->thumbnail;
                $post['categories'] = $post->categories;
                $post['date'] = $post->created_at->format(trans('model.dateformat.full'));
            }

            return $posts;
        });

        // Category pagination
        Route::get('{category}/paginate/{page}/{limit?}', function ($category, $page, $limit = 3) {
            $category = \App\Models\Category::findBySlug($category);
            $posts = \App\Models\Category::find($category->id)->posts()->take($limit)->skip(($page - 1) * $limit)->get();

            foreach($posts as $post){
                $post['thumbnail'] = $post->thumbnail;
                $post['categories'] = $post->categories;
            }

            return $posts;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\PostTranslation::where('title', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $object = \App\Models\Post::find($m->post_id);
                $data[$y]["id"] = $object->id;
                $data[$y]["name"] = $object->title;
                $y++;
            }

            return $data;
        });
    });

    // Package
    Route::group(['prefix' => 'package'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $packages = \App\Models\Package::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($packages as $package){
                $package['thumbnail'] = $package->thumbnail;
                $package['categories'] = $package->product->categories;
            }

            return $packages;
        });

        // Category pagination
        Route::get('{category}/paginate/{page}/{limit?}', function ($category, $page, $limit = 3) {
            $category = \App\Models\Category::findBySlug($category);
            $products = \App\Models\Category::find($category->id)->products()->take($limit)->skip(($page - 1) * $limit)->get();
            $products_id = [];

            foreach ($products as $product) {
                $products_id[] = $product->id;
            }

            $packages = \App\Models\Package::whereIn('product_id', $products_id)->get();

            foreach($packages as $package){
                $package['thumbnail'] = $package->thumbnail;
                $package['categories'] = $package->product->categories;
            }

            return $packages;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\ProductTranslation::where('name', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $object = \App\Models\Product::find($m->product_id);
                $data[$y]["id"] = $object->id;
                $data[$y]["name"] = $object->name;
                $y++;
            }

            return $data;
        });
    });

     // User
    Route::group(['prefix' => 'user'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
//            $users = \App\Models\User::take($limit)->skip(($page - 1) * $limit)->get();
            $users = \App\Models\User::findByRolePaginator(1, $limit, $page);

            foreach($users as $user){
                $user['name'] = $user->name;
            }

            return $users;
        });

         // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\User::findByName($query, 1);
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->first_name . ' ' . $m->last_name;
                $y++;
            }

            return $data;
        });
    });

     // Page
    Route::group(['prefix' => 'page'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $pages = \App\Models\Page::take($limit)->skip(($page - 1) * $limit)->get();
            foreach($pages as $page){
                dd($page->title);
                $page['date'] = $page->created_at->format(trans('model.dateformat.full'));
            }

            return $pages;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\PageTranslation::where('title', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $object = \App\Models\Page::find($m->page_id);
                $data[$y]["id"] = $object->id;
                $data[$y]["name"] = $object->title;
                $y++;
            }

            return $data;
        });
    });

      // Customer
    Route::group(['prefix' => 'customer'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
//            $customers = \App\Models\Customer::take($limit)->skip(($page - 1) * $limit)->get();
            $customers = \App\Models\User::findByRolePaginator(3, $limit, $page);

            foreach($customers as $customer){
                $customer['name'] = $customer->name;
                $customer['l_login'] = $customer->last_login->format(trans('model.dateformat.full'));
            }

            return $customers;
        });

         // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\User::findByName($query, 3);
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->first_name . ' ' . $m->last_name;
                $y++;
            }

            return $data;
        });
    });

      // Booking
    Route::group(['prefix' => 'booking'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $bookings = \App\Models\Booking::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($bookings as $booking){
                $booking['user_name'] = $booking->order->customer->user->name;
                $booking['customer_id'] = $booking->order->customer->id;
                $booking['package_id'] = $booking->package_id;
                $booking['product_name'] = $booking->package->product->name;
                if($booking->status == 1){
                    if(\Carbon\Carbon::now() > $booking->order->getDate()->addHours(24)){
                        $booking['pendingClass'] = 'label-danger';
                    }else if(\Carbon\Carbon::now() > $booking->order->getDate()->addHours(18)){
                        $booking['pendingClass'] = 'label-warning';
                    }else{
                        $booking['pendingClass'] = 'label-default';
                    }
                }
            }

            return $bookings;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\Booking::findAutoComplete($query);
//            print_r($main); exit;
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->order->customer->user->name;
                $y++;
            }

            return $data;
        });
    });

      // Order
    Route::group(['prefix' => 'order'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $orders = \App\Models\Order::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($orders as $order){
                $order['user_name'] = $order->customer->user->name;
                $order['customer_id'] = $order->customer->id;
                $order['date'] = $order->created_at;
                $order['total'] = $order->getTotal();
            }

            return $orders;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\Order::findAutoComplete($query);
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->customer->user->name;
                $y++;
            }

            return $data;
        });
    });

      // Promotion
    Route::group(['prefix' => 'promotion'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $promotions = \App\Models\Promotion::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($promotions as $promotion){
                $promotion['date'] = $promotion->created_at->format(trans('model.dateformat.full'));

            }

            return $promotions;
        });

         // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\Promotion::where('name', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->name;
                $y++;
            }

            return $data;
        });
    });

      // Blockouts
    Route::group(['prefix' => 'blockout'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $blockouts = \App\Models\Package::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($blockouts as $blockout){
                $blockout['name'] = $blockout->product->name;
                $blockout['date'] = $blockout->created_at->format(trans('model.dateformat.full'));

            }

            return $blockouts;
        });
    });

      // Coupon
    Route::group(['prefix' => 'coupon'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $coupons = \App\Models\Coupon::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($coupons as $coupon){
                $coupon['date'] = $coupon->created_at->format(trans('model.dateformat.full'));
            }

            return $coupons;
        });

         // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\Coupon::where('name', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->name;
                $y++;
            }

            return $data;
        });
    });

      // Partner
    Route::group(['prefix' => 'partner'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $partners = \App\Models\Partner::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($partners as $partner){
                $partner['name'] = $partner->user->name;
                $partner['email'] = $partner->user->email;
                $partner['l_login'] = is_null($partner->user->last_login) ? " - " : $partner->user->last_login->format(trans('model.dateformat.full'));
            }

            return $partners;
        });

         // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\User::findByName($query, 2);
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->first_name . ' ' . $m->last_name;
                $y++;
            }

            return $data;
        });
    });

      // Store
    Route::group(['prefix' => 'store'], function () {
        // Pagination
        Route::get('paginate/{page}/{limit?}', function ($page, $limit = 3) {
            $stores = \App\Models\Store::take($limit)->skip(($page - 1) * $limit)->get();

            foreach($stores as $store){
                $store['date'] = $store->created_at->format(trans('model.dateformat.full'));
            }

            return $stores;
        });

        // Autocomplete
        Route::get('search/{query}', function ($query) {
            $main = \App\Models\Store::where('name', 'LIKE', '%'.$query.'%')->get();
            $data = array();
            $y = 0;
            foreach ($main as $m) {
                $data[$y]["id"] = $m->id;
                $data[$y]["name"] = $m->name;
                $y++;
            }

            return $data;
        });
    });

    // Newsletter
    Route::group(['prefix' => 'newsletter'], function () {
        // Mailchimp Register
        Route::post('/{list}/register', function ($list) {
//            dd($_POST['email']);
            $MailChimp = new MailChimp('9ccd55d525a421b5404cc4bc2254d7fa-us13');
            $result = $MailChimp->post("lists/$list/members", [
                'email_address' => $_POST['email'],
                'status'        => 'subscribed',
            ]);
            if(isset($result['id'])){
               return array('msg' => 'Cadastro efetuado!');
            }else{
                return array('msg' => $result['title']);
            }

        });
    });

    // Admin privileges
    Route::group(['middleware' => 'auth.admin'], function () {
        // Set featured package
        Route::get('package/featured/set/{id}', function ($id) {
            return \App\Models\Package::setFeatured($id);
        });
    });
});
