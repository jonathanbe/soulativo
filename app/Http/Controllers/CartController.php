<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cart;
use App\Models\Package;
use App\Models\Promotion;

class CartController extends Controller
{
    /**
     * Show the products inside the cart
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'cart/list';
        $cart = Cart::contents();
        $total = 0;

        foreach($cart as $item){
            $item->package = Package::where('id', $item->id)->first();
            Promotion::products([$item->package]);
        }

        return view('cart.index')
            ->with('cart', $cart)
            ->with('frontend', $frontend)
            ->with('total', $total);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $qty = $input['booking']['adults'] + ((isset($input['booking']['childs'])) ? $input['booking']['childs'] : 0 );

        Cart::insert([
            'id' => $input['package_id'],
            'name' => $input['package_name'],
            'price' => $input['price'],
            'quantity' => $qty,
            'date' => $input['booking']['date'],
            'adults' => $input['booking']['adults'],
            'childs' => $input['booking']['childs']
        ]);

        return redirect('/cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $input = $request->input();
        $cart = Cart::contents();

        foreach(Cart::contents() as $key => $item){
            $item->date = $input['booking'][$key]['date'];
            $item->adults = $input['booking'][$key]['adults'];
            $item->childs = $input['booking'][$key]['childs'];
            $item->departure_location = $input['booking'][$key]['departure_location'];
            $item->departure_hour = $input['booking'][$key]['departure_hour'];
            $item->quantity = $item->adults + $item->childs;
        }

        return redirect('/checkout/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = $request->input();
        Cart::remove($input['CartID']);
    }
}
