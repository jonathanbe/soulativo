<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Page;
use App\Models\PageTranslation;
use Breadcrumbs;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $frontend['view'] = 'page/list';
        $pages = Page::all()->take($perPage);
        $deleted_pages = Page::onlyTrashed()->get();

        return view('admin/page/index')
            ->with('frontend', $frontend)
            ->with('pages', $pages)
            ->with('breadcrumb', 'page')
            ->with('perPage', $perPage)
            ->with('deleted_pages', $deleted_pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'page/form';

        return view('admin/page/create')
            ->with('breadcrumb', 'page.create')
            ->with('frontend', $frontend);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $this->validate($request,
//                PageTranslation::getValidation()
//        );
        $input = $request->all();
        // Saves Page
        $page = Page::create();
//        dd($page->id);

        //Saves Page Translate
        $page_translation = new PageTranslation();
        $input['page']['slug'] = strtolower(str_replace(' ', '-', $input['page']['slug']));
        $page_translation->fill($input['page']);
        $page_translation->page_id = $page->id;
        $page_translation->locale = config('app.locale');
        $page_translation->save();


        $request->session()->flash('status', trans('model.page.create'));
        return redirect('admin/page/edit/' . $page->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'page/form';
        $page = Page::find($id);
        $page->page = $page;

        return view('admin/page/edit')
            ->with('frontend', $frontend)
            ->with('model', $page)
            ->with('breadcrumb', 'page.edit')
            ->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $page = Page::findOrFail($id);

//        $this->validate($request, Store::getValidation());

        // Updates Page
        $page->fill($input['page'])->save();

        $request->session()->flash('status', trans('model.page.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified page from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of stores from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $page = Page::onlyTrashed()->find($list);
                }else{
                    $page = Page::find($list);
                }

                if(!$page){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $page->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $page->id;
                    }
                }else{
                    if(!$page->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $page->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $page = Page::withTrashed()->where('id', $list);
                if(!$page->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
