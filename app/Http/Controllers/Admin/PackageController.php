<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Models\ProductTranslation;
use App\Models\Package;
use App\Models\PackageTranslation;
use App\Models\Store;
use App\Models\Category;
use App\Models\CategoryTranslation;
use App\Models\Media;

use Storage;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $frontend['view'] = 'package/list';
        $packages = Package::all()->take($perPage);
        $deleted_packages = Package::onlyTrashed()->get();

        return view('admin.package.index')->with('packages', $packages)
            ->with('deleted_packages', $deleted_packages)
            ->with('breadcrumb', 'package')
            ->with('perPage', $perPage)
            ->with('frontend', $frontend);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'package/form';
        $stores = Store::all();
        $photos = Storage::files('images');
        $root = Category::findBySlug(Package::$root_slug);
        $categories = $root->getDescendants();

        return view('admin/package/create')
            ->with('frontend', $frontend)
            ->with('stores', $stores)
            ->with('breadcrumb', 'package.create')
            ->with('root', $root)
            ->with('categories', $categories)
            ->with('photos', $photos);
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request,
            array_merge(
                Product::getValidation(),
                Package::getValidation()
            )
        );
//        dd($input['product']['slug']);
        $input['product']['slug'] = strtolower(str_replace(' ', '-', $input['product']['slug']));
        $product = Product::create($input['product']);
        $package = new Package();

        $package->setAttribute('product_id', $product->id);
        $input['package']['product_id'] = $product->id;
        $package->fill($input['package']);
        $package->save();
        $product->categories()->sync((!empty($input['product']['categories'])) ? $input['product']['categories'] : []);

        $request->session()->flash('status', trans('model.customer.create'));

        return redirect('admin/package/edit/' . $package->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'package/form';
        $stores = Store::all();
        $package = Package::findOrFail($id);
        $gallery = Media::getByJson($package->gallery);
        $root = Category::findBySlug(Package::$root_slug);
        $categories = $root->getDescendants();
        $package->package = $package;

        return view('admin/package/edit')
            ->with('frontend', $frontend)
            ->with('package', $package)
            ->with('breadcrumb', 'package.edit')
            ->with('model', $package)
            ->with('stores', $stores)
            ->with('root', $root)
            ->with('product', $package->product)
            ->with('categories', $categories)
            ->with('gallery', $gallery);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
//        dd($input);

        $package = Package::findOrFail($input['package']['id']);
        $product = Product::findOrFail($package->product_id);

        $this->validate($request,
            array_merge(
                Product::getValidation(),
                Package::getValidation()
            )
        );

        if (empty($input['package']['adults_only']))
          $input['package']['adults_only'] = false;
        
        if (empty($input['package']['unity']))
          $input['package']['unity'] = false;

        $product->fill($input['product']);
        $product->categories()->sync((!empty($input['product']['categories'])) ? $input['product']['categories'] : []);
        $product->save();


        $package->fill($input['package']);
        $package->save();


        $request->session()->flash('status', trans('model.page.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $package = Package::onlyTrashed()->find($list);
                }else{
                    $package = Package::find($list);
                    if($package->featured == 1){
                        $package->featured = 0;
                        $package->save();
                        $idNext = $package->id + 1;
                        $nextPackage = Package::find($idNext);
                        if(!is_null($nextPackage)){
                            $nextPackage->featured = 1;
                            $nextPackage->save();
                        }else{
                            $idPrev = $package->id - 1;
                            $prevPackage = Package::find($idPrev);
                            $prevPackage->featured = 1;
                            $prevPackage->save();
                        }
                    }
                }

                if(!$package){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $package->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $package->id;
                    }
                }else{
                    if(!$package->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $package->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
    {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $package = Package::withTrashed()->where('id', $list);
                if(!$package->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
