<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Coupon;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'coupon/list';
        $perPage = 10;
        $coupons = Coupon::all()->take($perPage);
        $deleted_coupons = Coupon::onlyTrashed()->get();

        return view('admin/coupon/index')
            ->with('frontend', $frontend)
            ->with('perPage', $perPage)
            ->with('breadcrumb', 'coupon')
            ->with('coupons', $coupons)
            ->with('deleted_coupons', $deleted_coupons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'coupon/form';

        return view('admin/coupon/create')
            ->with('breadcrumb', 'coupon.create')
            ->with('frontend', $frontend);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $this->validate($request,
            Coupon::getValidation()
        );

        if($input['coupon']['key'] == ''){
            $input['coupon']['key'] = Coupon::generateCouponCode(7);
        }

        $coupon = Coupon::create();
        //type field = %(1) or $(0)
        $coupon->fill($input['coupon']);
        $coupon->save();

        $request->session()->flash('status', trans('model.coupon.create'));
        return redirect('admin/coupon/edit/' . $coupon->id);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'coupon/form';
        $coupon = Coupon::find($id);
        $coupon->coupon = $coupon;

        return view('admin/coupon/edit')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'coupon.edit')
            ->with('model', $coupon)
            ->with('coupon', $coupon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $coupon = Coupon::findOrFail($id);

        $this->validate($request,
            Coupon::getValidation()
        );

        // Updates Coupon
        $coupon->fill($input['coupon']);
        $coupon->save();

        $request->session()->flash('status', trans('model.coupon.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified Coupon from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of stores from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $coupon = Coupon::onlyTrashed()->find($list);
                }else{
                    $coupon = Coupon::find($list);
                }

                if(!$coupon){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $coupon->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $coupon->id;
                    }
                }else{
                    if(!$coupon->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $coupon->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $coupon = Coupon::withTrashed()->where('id', $list);
                if(!$coupon->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
