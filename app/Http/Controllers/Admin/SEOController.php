<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Page;
use App\Models\Package;
use App\Models\Category;
use App\Models\SEO;

class SEOController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $frontend['view'] = 'seo/list';
        $perPage = 10;
        $posts = Post::all()->take($perPage);
        $pages = Page::all()->take($perPage);
        $categories = Category::getNestedList('name', 'id', '–');
        $packages = Package::all()->take($perPage);
        return view('admin.seo.index')
            ->with('frontend', $frontend)
            ->with('posts', $posts)
            ->with('pages', $pages)
            ->with('breadcrumb', 'seo')
            ->with('perPage', $perPage)
            ->with('categories', $categories)
            ->with('packages', $packages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug, $id) {
        $input = $request->all();

        $seo = new SEO();
        $seo->fill($input['seo'])->save();


        switch ($slug):
            case 'post':
                $content = Post::find($id);
                $content->seo_id = $seo->id;
                $content->save();
                break;
            case 'page';
                $content = Page::find($id);
                $content->seo_id = $seo->id;
                $content->save();
                break;
            case 'package':
                $content = Package::find($id);
                $content->seo_id = $seo->id;
                $content->save();
                break;
            case 'cat':
                $content = Category::find($id);
                $content->seo_id = $seo->id;
                $content->save();
                break;
        endswitch;

        $request->session()->flash('status', trans('model.seo.create'));
        return redirect('admin/seo/' . $slug . '/edit/' . $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $id) {
        $seo = null;
        $model = null;
        switch ($slug) {
            case 'post':
                $model = \App\Models\Post::find($id);
                $title = $model->title;
                $seo = $model->seo;
                break;
            case 'page':
                $model = \App\Models\Page::find($id);
                $title = $model->title;
                $seo = $model->seo;
                break;
            case 'cat':
                $model = \App\Models\Category::find($id);
                $title = $model->name;
                $seo = $model->seo;
                break;
            case 'package':
                $model = \App\Models\Package::find($id);
                $title = $model->name;
                $seo = $model->seo;
                break;
        }

        if (!empty($seo))
            $seo->seo = $seo;
        return view('admin.seo.edit')
            ->with('seo', $seo)
            ->with('model', $model)
            ->with('breadcrumb', 'seo.edit')
            ->with('slug', $slug)
            ->with('title', $title)
            ->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, $id) {
        $input = $request->all();

        switch ($slug):
            case 'post':
                $content = Post::find($id);
                break;
            case 'page';
                $content = Page::find($id);
                break;
            case 'package':
                $content = Package::find($id);
                break;
            case 'category':
                $content = Category::find($id);
                break;
        endswitch;

        $seo = SEO::find($content->seo_id);
        $seo->fill($input['seo'])->save();


        $request->session()->flash('status', trans('model.seo.create'));
        return redirect('admin/seo/' . $slug . '/edit/' . $id);
    }

}
