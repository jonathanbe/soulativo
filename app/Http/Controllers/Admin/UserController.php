<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Breadcrumbs;


class UserController extends Controller
{
    /**
     * Display a listing of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $frontend['view'] = 'user/list';
        $users = User::findByRole(1)->take($perPage);
        $deleted_users = User::findByRole('1',true);
        return view('admin.user.index')
            ->with('users', $users)
            ->with('breadcrumb', 'user')
            ->with('deleted_users', $deleted_users)
            ->with('perPage', $perPage)
            ->with('frontend', $frontend);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'user/form';
        $roles = Role::all();

        return view('admin.user.create')
            ->with('breadcrumb', 'user.create')
            ->with('roles', $roles)
            ->with('frontend', $frontend);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            array_merge(
                User::getValidation(),
                ['user.password' => 'required|confirmed|min:6']
            )
        );
        $input = $request->all();

        $input['user']['password'] = bcrypt($input['user']['password']);

        // Saves user
        $user = User::create($input['user']);
        
        if($input['role'] != ''){
            $user->setRole($input['role']);
        }

        $request->session()->flash('status', trans('model.user.create'));
        return redirect('admin/user/edit/' . $user->id);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'user/form';

        $user = User::findOrFail($id);
        $user->user = $user;

        return view('admin.user.edit')
            ->with('frontend', $frontend)
            ->with('model', $user)
            ->with('breadcrumb', 'user.edit')
            ->with('user', $user);

    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $this->validate($request, User::getValidation());
        $input = $request->all();

        if(!empty($input['user']['password'])){
            $input['user']['password'] = bcrypt($input['user']['password']);
        }


        // Updates user
        $user->fill($input['user'])->save();

        $request->session()->flash('status', trans('model.user.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of users from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $user = User::onlyTrashed()->find($list);
                }else{
                    $user = User::find($list);
                }

                if(!$user){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $user->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $user->id;
                    }
                }else{
                    if(!$user->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $user->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $user = User::withTrashed()->where('id', $list);
                if(!$user->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
