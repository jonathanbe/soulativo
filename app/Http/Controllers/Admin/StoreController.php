<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Countries;
use App\Models\Store;
use App\Models\Address;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'store/list';
        $perPage = 10;
        $stores = Store::all()->take($perPage);
        $deleted_stores = Store::onlyTrashed()->get();

        return view('admin.store.index')
            ->with('frontend', $frontend)
            ->with('perPage', $perPage)
            ->with('breadcrumb', 'store')
            ->with('stores', $stores)
            ->with('deleted_stores', $deleted_stores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'store/form';

        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = $country['name'];
        }

        return view('admin.store.create')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'store.create')
            ->with('countries', $countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,Store::getValidation());
        $input = $request->all();


        //Saves Address
        $address = new Address();
        $address->fill($input['address']);
        $address->country_id = $input['address']['country_id'];
        $address->save();
        $address_id = $address->getKey();

        // Saves store
        $store = new Store();
        $store->name = $input['store']['name'];
        $store->address_id = $address_id;
        $store->slug = $input['store']['slug'];
        $store->document = $input['store']['document'];
        $store->save();

        $request->session()->flash('status', trans('model.store.create'));
        return redirect('admin/store/edit/' . $store->id);
//        dd($request->input());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'store/form';

        $store = Store::findOrFail($id);
        $store->store = $store;


        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = $country['name'];
        }

        return view('admin.store.edit')
            ->with('frontend', $frontend)
            ->with('countries', $countries)
            ->with('breadcrumb', 'store.edit')
            ->with('model', $store)
            ->with('address', $store->address)
            ->with('store', $store);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request['store']);
        $input = $request->all();
        $store = Store::findOrFail($id);
        $address = Address::find($request['store']['address_id']);

        $this->validate($request, Store::getValidation());

        // Updates address
        if(!is_null($address)){
            $address->fill($input['address'])->save();
        }else{
            $address = new Address();
            $address->fill($input['address'])->save();
            $input['store']['address_id'] = $address->id;
        }


        // Updates store
        $store->fill($input['store']);
        $store->address_id = $address->id;
        $store->save();



        $request->session()->flash('status', trans('model.store.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of stores from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $store = Store::onlyTrashed()->find($list);
                }else{
                    $store = Store::find($list);
                }

                if(!$store){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $store->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $store->id;
                    }
                }else{
                    if(!$store->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $store->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $store = Store::withTrashed()->where('id', $list);
                if(!$store->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
