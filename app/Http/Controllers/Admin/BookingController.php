<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Package;
use App\Models\Customer;
use App\Models\Order;
use Carbon\Carbon;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'booking/list';
        $perPage = 10;
        $bookings = Booking::all()->sortByDesc("created_at")->take($perPage);
        $deleted_bookings = Booking::onlyTrashed()->get();


        // Set different colors to pending status
        foreach($bookings as $booking){
            if($booking->status == 1){
                if(Carbon::now() > $booking->order->getDate()->addHours(24)){
                    $booking->pendingClass = 'label-danger';
                }else if(Carbon::now() > $booking->order->getDate()->addHours(18)){
                    $booking->pendingClass = 'label-warning';
                }else{
                    $booking->pendingClass = 'label-default';
                }
            }
        }

        return view('admin.booking.index')
            ->with('frontend', $frontend)
            ->with('bookings', $bookings)
            ->with('breadcrumb', 'booking')
            ->with('perPage', $perPage)
            ->with('deleted_bookings', $deleted_bookings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontend['view'] = 'booking/form';
        $packages = Package::all();
        $customers = Customer::all();

        return view('admin.booking.create')
            ->with('packages', $packages)
            ->with('frontend', $frontend)
                ->with('breadcrumb', 'booking.create')
            ->with('customers', $customers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        // Validates
        $this->validate($request, Booking::getValidation());

        // Get selected package data
        $package = Package::find($input['booking']['package_id']);

        // Creates an order
        $order = new Order();
        $order->fill($input['order'])->save();

        // Creates a booking
        $booking = new Booking();
        $booking->total = $package->product->getPrice('BRL');
        $booking->discount = $booking->total - $package->product->getDiscountedPrice(\App\Models\Product::$baseCurrency);
        $booking->status = 1;

        // Convert date to timestring
        $carbon = new Carbon($input['booking']['date']);
        $input['booking']['date'] = $carbon->toDateTimeString();

        // Updates booking
        $booking->order_id = $order->id;
        $booking->fill($input['booking'])->save();

        $request->session()->flash('status', trans('model.booking.create'));
        return redirect('admin/booking/edit/' . $booking->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'booking/form';
        $booking = Booking::find($id);
        $packages = Package::all();
        $customers = Customer::all();
        $booking->booking = $booking;

        return view('admin.booking.edit')
            ->with('booking', $booking)
            ->with('model', $booking)
                ->with('breadcrumb', 'booking.edit')
            ->with('packages', $packages)
            ->with('frontend', $frontend)
            ->with('customers', $customers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $booking = Booking::find($id);

        $this->validate($request, Booking::getValidation());

        $carbon = new Carbon($input['booking']['date']);
        $input['booking']['date'] = $carbon->toDateTimeString();

        // Updates booking
        $booking->fill($input['booking'])->save();

        $request->session()->flash('status', trans('model.booking.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
      * Remove multiple entries of stores from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $booking = Booking::onlyTrashed()->find($list);
                }else{
                    $booking = Booking::find($list);
                }

                if(!$booking){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $booking->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $booking->id;
                    }
                }else{
                    if(!$booking->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $booking->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $booking = Booking::withTrashed()->where('id', $list);
                if(!$booking->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
