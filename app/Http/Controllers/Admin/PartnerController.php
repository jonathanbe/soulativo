<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Countries;
use App\Models\Partner;
use App\Models\User;
use App\Models\Role;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'partner/list';
        $perPage = 10;
        $partners = User::findByRole('2')->take($perPage);
        $deleted_users = User::findByRole('2', true);

        return view('admin.partner.index')->with('partners', $partners)
                                          ->with('perPage', $perPage)
                                          ->with('breadcrumb', 'partner')
                                          ->with('deleted_users', $deleted_users)
                                          ->with('frontend', $frontend);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           $frontend['view'] = 'customer/form';

        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = $country['name'];
        }

        return view('admin.partner.create')
            ->with('breadcrumb', 'partner.create')
            ->with('frontend', $frontend)
            ->with('countries', $countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            array_merge(
                User::getValidation(),
                Partner::getValidation(),
                ['user.password' => 'required|confirmed|min:6']
            )
        );
        $input = $request->all();

        // Saves user
        $user = User::create($input['user']);
        $user->addRole('partner');

        // Saves partner
        $partner = new Partner();
        $partner->fill($input['partner']);
        $partner->id = $user->id;
        $partner->document = $input['partner']['document'];
        $partner->store_id = 1;
        $partner->save();

        $request->session()->flash('status', trans('model.user.create'));
        return redirect('admin/partner/edit/' . $user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $frontend['view'] = 'partner/form';

        $partner = Partner::findOrFail($id);
        $partner->partner = $partner;

        return view('admin.partner.edit')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'partner.edit')
            ->with('model', $partner)
            ->with('user', $partner->user)
            ->with('partner', $partner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $partner = Partner::find($id);

        $this->validate($request, array_merge(
                User::getValidation(),
                Partner::getValidation()
            ));
        $input = $request->all();

        // Updates user
        $user->fill($input['user'])->save();




        // Updates user
        $partner->fill($input['partner'])->save();

        $request->session()->flash('status', trans('model.partner.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         //
     }

     /**
      * Remove multiple entries of users from storage.
      *
      * @param  Request  $request
      * @return \Illuminate\Http\Response
      */
     public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $user = User::onlyTrashed()->find($list);
                }else{
                    $user = User::find($list);
                }

                if(!$user){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $user->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $user->id;
                    }
                }else{
                    if(!$user->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $user->id;
                    }
                }
            }
            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_ids' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $user = User::withTrashed()->where('id', $list);
                if(!$user->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_id' => $error
            ]);
        }
    }
}
