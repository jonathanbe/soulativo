<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryTranslation;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $frontend['view'] = 'category/list';
        $root = Category::findBySlug($slug);
        $categories = $root->getDescendants()->all();
        $deleted_categories = Category::onlyTrashed()->get();

        return view('admin/category/index')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'category')
            ->with('model', Category::class)
            ->with('slug', $slug)
            ->with('categories', $categories)
            ->with('deleted_categories', $deleted_categories)
            ->with('root', $root);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($slug)
    {
        $frontend['view'] = 'category/form';
        $root = Category::findBySlug($slug);
        $categories = $root->getDescendants();

        return view('admin/category/create')
            ->with('frontend', $frontend)
            ->with('breadcrumb', 'category.create')
            ->with('model', 'Category')
            ->with('slug', $slug)
            ->with('root', $root)
            ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $input = $request->all();
        $root = Category::findBySlug($slug);
        $parent = Category::find($input['category']['parent_id']);


        $child = new Category();
        $child->save();
        $child->makeChildOf($parent);

        $translation = new CategoryTranslation();
        $translation->category_id = $child->id;
        $translation->name = $input['category']['name'];
        $translation->slug = strtolower(str_replace(' ', '-', $input['category']['name']));
        $translation->locale = config('app.locale');
        $translation->save();

        if($request->ajax()) { //If created from sidebar
            $return = array('id' => $child->id, 'name' => $translation->name, 'depth' => $child->depth, 'parent_id' => $child->parent_id);

            echo json_encode([ //TODO: Preciso de pelo menos o ID e Nome da categoria pra eu popular a tabela
                'error' => false, //or true
                'input' => $return
            ]);
            exit;
        } else { //If created from form page
            $this->validate($request, Category::getValidation());

            $request->session()->flash('status', trans('model.category.create'));
            return redirect('admin/' . $slug . '/category/edit/' . $child->id);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $id)
    {
        $frontend['view'] = 'category/form';

        $category = Category::find($id);
        $root = Category::findBySlug($slug);
        $categories = $root->getDescendants();

        $category->category = $category;

        return view('admin/category/edit')
            ->with('category', $category)
            ->with('breadcrumb', 'category.edit')
            ->with('model', $category)
            ->with('slug', $slug)
            ->with('categories', $categories)
            ->with('root', $root)
            ->with('frontend', $frontend);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, $id)
    {
        $input = $request->all();
        $category = Category::find($id);
        $parent = Category::find($input['category']['parent_id']);
        $category->fill($input['category'])->save();
        $category->makeChildOf($parent);

        $request->session()->flash('status', trans('model.category.edit'));
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove multiple entries of users from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyMultiple(Request $request)
     {
         if($request->ajax()) {
             $success = [];
             $error = [];
             $exception = [];

            foreach($request['checkbox'] as $list){
                if(isset($request['trash_delete'])){
                    $category = Category::onlyTrashed()->find($list);
                }else{
                    $category = Category::find($list);
                }

                if(!$category){
                    $error[] = $list;
                    continue;
                }
                if(isset($request['trash_delete'])){
                    try
                    {
                        $category->forceDelete();
                    }
                    catch(Exception $ex)
                    {
                       $error[] = $list;
                       $exception[] = $ex->getMessage();
                       continue;
                    }
                    if(!isset($ex)){
                        $success[] = $category->id;
                    }
                }else{
                    if(!$category->destroy($list)){
                        $error[] = $list;
                        continue;
                    }else{
                        $success[] = $category->id;
                    }
                }
            }

            echo json_encode([
                'input' => $request->all(),
                'success_id' => $success,
                'exception' => $exception,
                'error_id' => $error
            ]);
        }
    }

    public function restore(Request $request)
     {
         if($request->ajax()) {
            $success = [];
            $error = [];
            foreach($request['checkbox'] as $list){
                $category = Category::withTrashed()->where('id', $list);
                if(!$category->restore()){
                $error[] = $list;
                        continue;
                }else{
                    $success[] = $list;
                }
            }
            echo json_encode([
                'error' => false, //or true
                'input' => $request->all(),
                'success_id' => $success,
                'error_ids' => $error
            ]);
        }
    }
}
