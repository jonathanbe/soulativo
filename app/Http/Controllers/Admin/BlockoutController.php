<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Blockout;
use App\Models\Package;

class BlockoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'blockout/list';
        $perPage = 10;
        $packages = Package::all()->take($perPage);

        return view('admin.blockout.index')
            ->with('perPage', $perPage)
            ->with('breadcrumb', 'blockout')
            ->with('packages', $packages)
            ->with('frontend', $frontend);
    }

    /**
     * Show the form for viewing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);
        $frontend['view'] = 'blockout/form';

        $blockout = new Blockout();
        $blockouts = $blockout->getBlockouts($id);

        return view('admin.blockout.show')
            ->with('package', $package)
            ->with('breadcrumb', 'blockout.show')
            ->with('model', $package)
            ->with('blockouts', $blockouts)
            ->with('frontend', $frontend);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  YYYY-MM-DD   date
     * @param  int          id
     * @return \Illuminate\Http\Response
     */
    public function store($date, $id)
    {
        // Faça uma checagem se a data do blockout já existe para o pacote,
        // Antes de inserir no banco de dados; Pois existe o risco de múltiplas chamadas ajax

        $checkDateExists = Blockout::where('date', '=', $date)->where('package_id', '=', $id)->get();
        if(count($checkDateExists) != 1){
            $blockout = new Blockout();
            $blockout->package_id = $id;
            $blockout->date = $date;
            $blockout->save();
        }

        return [
            'date' => $date,
            'error' => false
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  YYYY-MM-DD   date
     * @param  int          id
     */
    public function destroy($date, $id)
    {
        $checkDateExists = Blockout::where('date', '=', $date)->where('package_id', '=', $id)->take(1)->delete();

        return [
            'date' => $date,
            'error' => false
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $package = Package::find($id);

        $blockoutsArray = json_decode($input['package']['blockouts']);
        $existingBlockouts = $package->blockouts;
        foreach($blockoutsArray as $date){
            $date = explode('/', $date);
            $dateFormated = $date[2].'-'.$date[0].'-'.$date[1];
            $checkDateExists = Blockout::where('date', '=', $dateFormated)->get();
            if(count($checkDateExists) != 1){
                $blockout = new Blockout();
                $blockout->package_id = $id;
                $blockout->date = $dateFormated;
                $blockout->save();
            }
        }

        $request->session()->flash('status', trans('model.blockout.edit'));
        return redirect()->back();
    }
}
