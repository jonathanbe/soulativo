<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Mail;

use App\Models\Package;

class AdminController extends Controller
{
    public function dashboard ()
    {
        $frontend['view'] = 'dashboard/index';
        $packages = Package::all();
        $package1 = Package::where('position', '=', 1)->get();
        if(!isset($package1[0])){ $package1 = new Package(); }else{ $package1 = $package1[0]; }
        $package2 = Package::where('position', '=', 2)->get();
        if(!isset($package2[0])){ $package2 = new Package(); }else{ $package2 = $package2[0]; }
        $package3 = Package::where('position', '=', 3)->get();
        if(!isset($package3[0])){ $package3 = new Package(); }else{ $package3 = $package3[0]; }
        


        return view('admin.dashboard.index')
            ->with('packages', $packages)
            ->with('package1', $package1)
            ->with('package2', $package2)
            ->with('package3', $package3)
            ->with('breadcrumb', 'dashboard')
            ->with('frontend', $frontend);
    }

    public function setFeatured($id, $position)
    {
        $package = Package::find($id);
        $returnUpdate = Package::where('position', '=',$position)->update(array('position' => null));
        $package->position = $position;
        $package->save();

        
        $package['product'] = $package->product;
        $package['thumbnail'] = $package->thumbnail;

        return [
            'package' => $package,
            'error' => false
        ];
    }
}
