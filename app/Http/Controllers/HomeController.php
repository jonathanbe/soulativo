<?php

namespace App\Http\Controllers;

use Gate;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Package;
use App\Models\Promotion;
use App\Models\Product;
use App\Models\Post;
use App\Models\Page;
use Cart;
use Session;
use Mail;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $frontend['view'] = 'index/home';
        $frontend['top-class'] = '-transparent';
        $packagesDestaque = Package::where('featured', '=', 1)->get();
        Promotion::products($packagesDestaque); // Creates a promotion into the passed products
        $arrayIdsPackages = array($packagesDestaque[0]->id);

        $packages = Package::whereNotIn('id', $arrayIdsPackages)->take(3)->get();
        Promotion::products($packages);
        $recentPosts = Post::orderBy('created_at', 'desc')->take(6)->get();

        $package1 = Package::where('position', '=', 1)->get();
        if(!isset($package1[0])){ $package1 = new Package(); }else{ $package1 = $package1[0]; }
        $package2 = Package::where('position', '=', 2)->get();
        if(!isset($package2[0])){ $package2 = new Package(); }else{ $package2 = $package2[0]; }
        $package3 = Package::where('position', '=', 3)->get();
        if(!isset($package3[0])){ $package3 = new Package(); }else{ $package3 = $package3[0]; }

        return view('home.index')
            ->with('package_destaque', $packagesDestaque[0])
            ->with('packages', $packages)
            ->with('package1', $package1)
            ->with('package2', $package2)
            ->with('package3', $package3)
            ->with('recentPosts', $recentPosts)
            ->with('frontend', $frontend);
    }

    public function login()
    {
        // If user is already loggef in, redirects the user to the right place
        if(Auth::check()){
            return redirect((!empty($_GET['redirect'])) ? $_GET['redirect'] : '/' );
        }

        $frontend['view'] = 'index/login';
        $frontend['top-class'] = '-transparent';
        $packagesDestaque = Package::where('featured', '=', 1)->get();
        Promotion::products($packagesDestaque); // Creates a promotion into the passed products
        $arrayIdsPackages = array($packagesDestaque[0]->id);

        $packages = Package::whereNotIn('id', $arrayIdsPackages)->take(3)->get();
        Promotion::products($packages);
        $recentPosts = Post::orderBy('created_at', 'desc')->take(6)->get();

        $package1 = Package::where('position', '=', 1)->get();
        if(!isset($package1[0])){ $package1 = new Package(); }else{ $package1 = $package1[0]; }
        $package2 = Package::where('position', '=', 2)->get();
        if(!isset($package2[0])){ $package2 = new Package(); }else{ $package2 = $package2[0]; }
        $package3 = Package::where('position', '=', 3)->get();
        if(!isset($package3[0])){ $package3 = new Package(); }else{ $package3 = $package3[0]; }

        return view('home.index')
            ->with('package_destaque', $packagesDestaque[0])
            ->with('packages', $packages)
            ->with('package1', $package1)
            ->with('package2', $package2)
            ->with('package3', $package3)
            ->with('recentPosts', $recentPosts)
            ->with('frontend', $frontend)
            ->with('redirectUrl', ((!empty($_GET['redirect']))) ? $_GET['redirect'] : '') ;
    }

    public function contact(Request $request)
    {
        $page = Page::findBySlug('contato');
        $input = $request->all();
        if(!empty($input)){
          //  dd($input);
            Mail::send('emails.contact', ['input' => $input], function ($m) use ($input) {
                $m->from($input['email'], $input['name']);
                $m->to('contato@soulativo.com.br', 'SoulAtivo')->subject('Email de Contato');
            });

            $request->session()->flash('status', 'Email enviado com sucesso!');
        }
        return view('home.contact')->with('page', $page);
    }

    public function switchLanguage($language)
    {
        Session::put('locale', $language);
        return redirect()->back();
    }

    public function switchCurrency($currency)
    {
        Session::put('currency', $currency);
        return redirect()->back();
    }
}
