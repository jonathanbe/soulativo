<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Post;
use App\Models\Media;
use App\Models\Category;
use Session;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontend['view'] = 'blog/list';
        $posts = Post::take(3)->skip(1)->get();
        $featured = Post::all()->first();
        $categories = Category::findBySlug('posts');

        return view('blog.index')
            ->with('posts', $posts)
            ->with('categories', $categories)
            ->with('featured', $featured)
            ->with('frontend', $frontend);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, Request $request)
    {
        
        $post = Post::findBySlug($slug);
        Session::put('seo', $post->seo);
        $arrayIdsPosts = array($post->id);
        $recentPosts = Post::orderBy('created_at', 'desc')->whereNotIn('id', $arrayIdsPosts)->take(6)->get();
        $gallery = Media::find($post->thumbnail);
        $categories = Category::findBySlug('posts');
        
        $currentUrl = $request->url();
       
        return view('blog.show')
            ->with('post', $post)
            ->with('currentUrl', $currentUrl)
            ->with('gallery', $gallery)
            ->with('recentPosts', $recentPosts)
            ->with('categories', $categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $frontend['view'] = 'blog/list';

        $categories = Category::findBySlug('posts');

        $category = Category::findBySlug($slug);
        $posts = Post::getAllByCategorySlug($slug);

        $featured = $posts[0];

        return view('blog.index')
            ->with('categories', $categories)
            ->with('category', $category)
            ->with('slug', $slug)
            ->with('posts', $posts)
            ->with('featured', $featured)
            ->with('frontend', $frontend);
    }
}
