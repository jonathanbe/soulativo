<?php

namespace App\Http\Controllers;

use Gate;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Media;
use App;

class MediaController extends Controller
{
    function upload(Request $request){
        if($request->ajax()) {
            $images = [];

            foreach($request->all()['files'] as $file){
                $error = false;
                try{
                    $newFilename = rand(1,999999999999) . "." . $file->getClientOriginalExtension();
                    $info = Media::upload($file, $newFilename);

                    $media = new Media();
                    $media->filename = $newFilename;
                    $media->title = "";
                    $media->caption = "";
                    $media->size = $file->getSize();
                    $media->save();

                    $images[$media->id] = [
                        'media' => $media,
                        'width' => $info[0],
                        'height' => $info[1],
                        'error' => $error
                    ];

                } catch(\Exception $e){
                    $error = true;
                    $images[]['error'] = $error;
                }
            }

            echo json_encode([
                'images' => $images
            ]);
            exit;
        }
    }

    function editorUpload(Request $request){
        if($request->ajax()) {
            $file = $request->all()['file'];
            $info = Media::upload($file);

            $media = new Media();
            $media->filename = $file->getClientOriginalName();
            $media->title = "";
            $media->caption = "";
            $media->size = $file->getSize();
            $media->save();

            echo '/images/' . $media->filename;
            exit;
        }
    }

    function getImages(){
        $medias = Media::orderBy('id', 'asc')->get();
        $images = [];
        $error = false;

        foreach($medias as $media){
            $images[] = [
                'media' => $media,
                'width' => 0,
                'height' => 0,
                'error' => $error
            ];
        }

        return json_encode(['images' => $images]);
    }

    function image($path, \League\Glide\Server $server){
        return $server->outputImage($path, $_GET);
    }

    function file($path){
        //
    }
}
