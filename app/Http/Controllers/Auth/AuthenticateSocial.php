<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Contracts\Auth\Guard;
use Request;
use Socialite;
use App\Models\User;
use Auth;

trait AuthenticateSocial
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider = null)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider = null)
    {
        $social = $this->getSocialuser($provider);

        switch ($provider) {
            case 'facebook':
                $user = $this->handleFacebook($social);
                break;

            case 'twitter':
                $user = $this->handleTwitter($social);
                break;
        }

        Auth::login($user, true);

        return redirect()->intended($this->redirectPath());
    }

    public function getSocialUser($provider)
    {
        return Socialite::driver($provider)->user();
    }

    public function handleFacebook($social)
    {
        $user = new User();
        $user = $user->findByUsernameOrCreate($social);
        $user->first_name = $social->user['first_name'];
        $user->last_name = $social->user['last_name'];
        $user->email = $social->user['email'];
        $user->provider = 'facebook';
        $user->provider_id = $social->user['id'];
        $user->save();

        return $user;
    }

    public function handleTwitter($social)
    {
        $user = new User();
        $social->user['email'] = $social->email;
        $social->user['name'] = $social->name;
        $user = $user->findByUsernameOrCreate($social);
        $user->first_name = $social['name'];
        $user->email = $social['email'];
        $user->provider = 'twitter';
        $user->provider_id = $social->id;
        $user->save();

        return $user;
    }
}
