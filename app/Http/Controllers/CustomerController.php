<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers;
use App\Models\Customer;
use App\Models\Address;
use Carbon\Carbon;
use Countries;
use Validator;

class CustomerController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user = Auth::user();
         $input = $request->all();
         
         if(!empty($input['first_name'])){
             $user->first_name = $input['first_name'];
             $user->last_name = $input['last_name'];
             $user->email = $input['email'];
             if(!is_null($input['password'])){
                $user->password = bcrypt($input['password']);
             }
             $user->save();
         }
         
         // Convert birthday date
         try {
             
             $input['customer']['birthday'] = Carbon::parse(str_replace("/", "-", $input['customer']['birthday']));
         } catch(\Exception $x) {
             $input['customer']['birthday'] = null;
         }

         $country = Countries::getOne($input['address']['country_id']);

         // Validations
         $validator = $this->validate($request, array_merge(
             Address::getValidation(),
             Customer::getValidation(),
             ['customer.telephone' => ($country['iso_3166_2'] == 'BR') ? 'required|phone:BR' : 'required'],
             ['customer.document' => ($country['iso_3166_2'] == 'BR') ? 'required|cpf' : 'required']
         ));

         // If user do not has customer information
         if(!$user->hasRole('customer')){
            //Saves Address
            $address = new Address();
            $address->fill($input['address']);
            $address->country_id = $input['address']['country_id'];
            $address->save();
            $address_id = $address->getKey();

            // Saves Customer
            $customer = new Customer();
            $customer->fill($input['customer']);
            $customer->id = $user->id;
            $customer->address_id = $address_id;
            $customer->save();
            
            // Adds customer role to the user
            $user->addRole('customer');
        } else {
            $customer = Customer::find($user->id);
            if(is_null($customer)){
                //Saves Address
                $address = new Address();
                $address->fill($input['address']);
                $address->country_id = $input['address']['country_id'];
                $address->save();
                $address_id = $address->getKey();

                // Saves Customer
                $customer = new Customer();
                $customer->fill($input['customer']);
                $customer->id = $user->id;
                $customer->address_id = $address_id;
                $customer->save();
            }else{
                $address = Address::find($customer->address_id);
            }

            //Update Address
            $address->fill($input['address'])->save();

            // Updates Costumer
            $customer->fill($input['customer'])->save();
        }

        if(!empty($input['edit-profile'])){
            $request->session()->flash('status', trans('model.customer.create'));
            return redirect('customer/profile/edit/');
        }else{
            // Creates an order
            return app('App\Http\Controllers\OrderController')->store();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $customer = Auth::user()->customer;
        if(is_null($customer)){
            return redirect('/customer/profile/edit');
        }
        $orders = $customer->orders;

        return view('customer.profile')
            ->with('customer', $customer)
            ->with('orders', $orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $frontend['view'] = 'customer/edit';
        $countries[null] = trans('model.country.select');
        foreach(Countries::getList() as $key => $country){
            $countries[$key] = array('name' => $country['name'], 'calling_code' => $country['calling_code']);
        }

        $user = Auth::user();
        $customer = Customer::find($user->id);

        if(!empty($customer)){
            if($customer->address->country->calling_code != 55){
                $customer->formatedBirthday = date('Y-m-d', strtotime(new Carbon($customer->birthday)));
            }else{
                $customer->formatedBirthday = date('d/m/Y', strtotime(new Carbon($customer->birthday)));
            }
            $customer->customer = $customer;
        }

        return view('customer.edit')
            ->with('countries', $countries)
            ->with('customer', $customer)
            ->with('user', $user)
            ->with('frontend', $frontend);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();


         $user->first_name = $input['first_name'];
         $user->last_name = $input['last_name'];
         $user->email = $input['email'];
         if(!is_null($input['password'])){
            $user->password = bcrypt($input['password']);
         }
         $user->save();

         // Convert birthday date
         try {
             $input['customer']['birthday'] = Carbon::parse(str_replace("/", "-", $input['customer']['birthday']));
         } catch(\Exception $x) {
             $input['customer']['birthday'] = null;
         }

         // Validations
         $this->validate($request, array_merge(
             Address::getValidation(),
             Customer::getValidation()
         ));

        $customer = Customer::find($user->id);
        $address = Address::find($customer->address_id);

        //Update Address
        $address->fill($input['address'])->save();

        // Updates Costumer
        $customer->fill($input['customer'])->save();

        $request->session()->flash('status', trans('model.customer.saved'));
        return redirect('customer/profile/edit/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

}
