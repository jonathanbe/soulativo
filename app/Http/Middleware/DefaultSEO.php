<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Models\SEO;

class DefaultSEO
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $seo = new SEO();
        $seo->title = "Soulativo";
        $seo->description = "Quer descobrir o melhor do Rio de Janeiro? Vem com a Soulativo! Conheça passeios incríveis para você amar ainda mais Cidade Maravilhosa.";
        $seo->keywords = "soulativo,turismo,entretenimento, passagens, viagens, avião, tour, aventura, passeios, helicoptero";
        Session::put('seo', $seo);
        
        return $next($request);
    }
}
