<?php

namespace App\Http\Middleware;

use Closure, Session, Auth;

class Currency
{
    /**
    * The availables languages.
    *
    * @array $languages
    */
   protected $currencies = [
       'real' => [
           'name' => 'BRL',
           'prefix' => 'R$'
       ],
       'dollar' => [
           'name' => 'USD',
           'prefix' => '$'
       ],
       'euro' => [
           'name' => 'EUR',
           'prefix' => '€'
       ]
   ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('currency')){
            Session::put('currency', 'real');
        }

        config(['app.currency' => $this->currencies[Session::get('currency')]]);

        return $next($request);
    }
}
