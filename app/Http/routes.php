<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

    /* Admin Panel */
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', function () { return redirect('/admin/login'); });
        Route::get('/login', 'Admin\Auth\AuthController@getLogin');
        Route::get('/logout', 'Admin\Auth\AuthController@logout');
        Route::post('/login', 'Admin\Auth\AuthController@postLogin');

        Route::group(['middleware' => 'auth.admin'], function () {
            Route::get('/dashboard', ['uses' => 'Admin\AdminController@dashboard', 'as' => 'dashboard']);
            Route::get('/dashboard/package/{id}/featured/{position}', 'Admin\AdminController@setFeatured');

            /* User */
            Route::get('/users', ['uses' =>'Admin\UserController@index', 'as' => 'user']);
            Route::get('/user/create', ['uses' => 'Admin\UserController@create', 'as' => 'user.create']);
            Route::post('/user/create', 'Admin\UserController@store');
            Route::get('/user/edit/{id}', ['uses' => 'Admin\UserController@edit', 'as' => 'user.edit']);
            Route::post('/user/edit/{id}', 'Admin\UserController@update');
            Route::post('/user/destroy', 'Admin\UserController@destroyMultiple');
            Route::post('/user/destroy/{id}', 'Admin\UserController@destroy');
            Route::post('/user/restore', 'Admin\UserController@restore');

            /* Customer */
            Route::get('/customers', ['uses'  => 'Admin\CustomerController@index', 'as' => 'customer']);
            Route::get('/customer/create', ['uses'  => 'Admin\CustomerController@create', 'as' => 'customer.create']);
            Route::post('/customer/create', 'Admin\CustomerController@store');
            Route::get('/customer/edit/{id}', ['uses'  => 'Admin\CustomerController@edit', 'as' => 'customer.edit']);
            Route::post('/customer/edit/{id}', 'Admin\CustomerController@update');
            Route::post('/customer/destroy', 'Admin\CustomerController@destroyMultiple');
            Route::post('/customer/destroy/{id}', 'Admin\CustomerController@destroy');
            Route::post('/customer/restore', 'Admin\CustomerController@restore');

            /* Partner */
            Route::get('/partners', ['uses'  => 'Admin\PartnerController@index', 'as' => 'partner']);
            Route::get('/partner/create', ['uses'  => 'Admin\PartnerController@create', 'as' => 'partner.create']);
            Route::post('/partner/create', 'Admin\PartnerController@store');
            Route::get('/partner/edit/{id}', ['uses'  => 'Admin\PartnerController@edit', 'as' => 'partner.edit']);
            Route::post('/partner/edit/{id}', 'Admin\PartnerController@update');
            Route::post('/partner/destroy', 'Admin\PartnerController@destroyMultiple');
            Route::post('/partner/destroy/{id}', 'Admin\PartnerController@destroy');
            Route::post('/partner/restore', 'Admin\PartnerController@restore');

            /* Page */
            Route::get('/pages', ['uses' => 'Admin\PageController@index', 'as' => 'page']);
            Route::get('/page/create', ['uses' => 'Admin\PageController@create', 'as' => 'page.create']);
            Route::post('/page/create', 'Admin\PageController@store');
            Route::get('/page/edit/{id}', ['uses' => 'Admin\PageController@edit', 'as' => 'page.edit']);
            Route::post('/page/edit/{id}', 'Admin\PageController@update');
            Route::post('/page/destroy', 'Admin\PageController@destroyMultiple');
            Route::post('/page/destroy/{id}', 'Admin\PageController@destroy');
            Route::post('/page/restore', 'Admin\PageController@restore');

            /* Post */
            Route::get('/posts', ['uses'  => 'Admin\PostController@index', 'as' => 'post']);
            Route::get('/post/create', ['uses'  => 'Admin\PostController@create', 'as' => 'post.create']);
            Route::post('/post/create', 'Admin\PostController@store');
            Route::get('/post/edit/{id}', ['uses'  => 'Admin\PostController@edit', 'as' => 'post.edit']);
            Route::post('/post/edit/{id}', 'Admin\PostController@update');
            Route::post('/post/destroy', 'Admin\PostController@destroyMultiple');
            Route::post('/post/destroy/{id}', 'Admin\PostController@destroy');
            Route::post('/post/restore', 'Admin\PostController@restore');

            /* Stores */
            Route::get('/stores', ['uses'  => 'Admin\StoreController@index', 'as' => 'store']);
            Route::get('/store/create', ['uses'  => 'Admin\StoreController@create', 'as' => 'store.create']);
            Route::post('/store/create', 'Admin\StoreController@store');
            Route::get('/store/edit/{id}', ['uses'  => 'Admin\StoreController@edit', 'as' => 'store.edit']);
            Route::post('/store/edit/{id}', 'Admin\StoreController@update');
            Route::post('/store/destroy', 'Admin\StoreController@destroyMultiple');
            Route::post('/store/destroy/{id}', 'Admin\StoreController@destroy');
            Route::post('/store/restore', 'Admin\StoreController@restore');

            /* Package */
            Route::get('/packages', ['uses'  => 'Admin\PackageController@index', 'as' => 'package']);
            Route::get('/package/create', ['uses'  => 'Admin\PackageController@create', 'as' => 'package.create']);
            Route::post('/package/create', 'Admin\PackageController@store');
            Route::get('/package/edit/{id}', ['uses'  => 'Admin\PackageController@edit', 'as' => 'package.edit']);
            Route::post('/package/edit/{id}', 'Admin\PackageController@update');
            Route::post('/package/destroy', 'Admin\PackageController@destroyMultiple');
            Route::post('/package/destroy/{id}', 'Admin\PackageController@destroy');
            Route::post('/package/restore', 'Admin\PackageController@restore');

            /* Category */
            Route::get('/{slug}/categories', ['uses'  => 'Admin\CategoryController@index', 'as' => 'category']);
            Route::get('/{slug}/category/create', ['uses'  => 'Admin\CategoryController@create', 'as' => 'category.create']);
            Route::post('/{slug}/category/create', 'Admin\CategoryController@store');
            Route::post('/{slug}/category/create/aside', 'Admin\CategoryController@store');
            Route::get('/{slug}/category/edit/{id}', ['uses'  => 'Admin\CategoryController@edit', 'as' => 'category.edit']);
            Route::post('/{slug}/category/edit/{id}', 'Admin\CategoryController@update');
            Route::post('/{slug}/category/destroy', 'Admin\CategoryController@destroyMultiple');
            Route::post('/{slug}/category/destroy/{id}', 'Admin\CategoryController@destroy');
            Route::post('/{slug}/category/restore', 'Admin\CategoryController@restore');

            /* Booking */
            Route::get('/bookings', ['uses'  => 'Admin\BookingController@index', 'as' => 'booking']);
            Route::get('/booking/create', ['uses'  => 'Admin\BookingController@create', 'as' => 'booking.create']);
            Route::post('/booking/create', 'Admin\BookingController@store');
            Route::get('/booking/edit/{id}', ['uses'  => 'Admin\BookingController@edit', 'as' => 'booking.edit']);
            Route::post('/booking/edit/{id}', 'Admin\BookingController@update');
            Route::post('/booking/destroy', 'Admin\BookingController@destroyMultiple');
            Route::post('/booking/destroy/{id}', 'Admin\BookingController@destroy');

            /* Promotion */
            Route::get('/promotions', ['uses'  => 'Admin\PromotionController@index', 'as' => 'promotion']);
            Route::get('/promotion/create', ['uses'  => 'Admin\PromotionController@create', 'as' => 'promotion.create']);
            Route::post('/promotion/create', 'Admin\PromotionController@store');
            Route::get('/promotion/edit/{id}', ['uses'  => 'Admin\PromotionController@edit', 'as' => 'promotion.edit']);
            Route::post('/promotion/edit/{id}', 'Admin\PromotionController@update');
            Route::post('/promotion/destroy', 'Admin\PromotionController@destroyMultiple');
            Route::post('/promotion/destroy/{id}', 'Admin\PromotionController@destroy');

            /* Order */
            Route::get('/orders', ['uses'  => 'Admin\OrderController@index', 'as' => 'order']);
            Route::get('/order/show/{id}', ['uses'  => 'Admin\OrderController@show', 'as' => 'order.show']);
            Route::post('/order/{id}/send/email', 'Admin\OrderController@sendMail');
            Route::post('/order/{id}/create/coupon', 'Admin\OrderController@applyCoupon');
            Route::post('/order/{id}/remove/coupon', 'Admin\OrderController@removeCoupon');

            /* Media */
            Route::post('/media/update/{id}', 'Admin\MediaController@update');
            Route::post('/media/destroy/{id}', 'Admin\MediaController@destroy');

            /* SEO */
            Route::get('/seo', ['uses' => 'Admin\SEOController@index', 'as' => 'seo']);
            Route::get('/seo/{slug}/edit/{id}', ['uses' => 'Admin\SEOController@edit','as' => 'seo.edit']);
            Route::post('/seo/{slug}/create/{id}', 'Admin\SEOController@store');
            Route::post('/seo/{slug}/edit/{id}', 'Admin\SEOController@update');

            /* Coupon */
            Route::get('/coupons', ['uses'  => 'Admin\CouponController@index', 'as' => 'coupon']);
            Route::get('/coupon/create', ['uses'  => 'Admin\CouponController@create', 'as' => 'coupon.create']);
            Route::post('/coupon/create', 'Admin\CouponController@store');
            Route::get('/coupon/edit/{id}', ['uses'  => 'Admin\CouponController@edit', 'as' => 'coupon.edit']);
            Route::post('/coupon/edit/{id}', 'Admin\CouponController@update');
            Route::post('/coupon/destroy', 'Admin\CouponController@destroyMultiple');
            Route::post('/coupon/destroy/{id}', 'Admin\CouponController@destroy');
            Route::post('/coupon/restore', 'Admin\CouponController@restore');

            /* Blockout */
            Route::get('/blockouts', ['uses'  => 'Admin\BlockoutController@index', 'as' => 'blockout']);
            Route::get('/blockout/show/package/{id}', ['uses'  => 'Admin\BlockoutController@show', 'as' => 'blockout.show']);
            Route::post('/blockout/show/package/{id}', 'Admin\BlockoutController@update');
            Route::get('/blockout/create/{date}/package/{id}', 'Admin\BlockoutController@store');
            Route::get('/blockout/destroy/{date}/package/{id}', 'Admin\BlockoutController@destroy');
        });
    });

    /* Lang & Currency */
    Route::get('/lang/{language}', 'HomeController@switchLanguage');
    Route::get('/currency/{currency}', 'HomeController@switchCurrency');

    /* Uploads */
    Route::get('images/{path}', 'MediaController@image');
    Route::get('media/images', 'MediaController@getImages');
    Route::post('media/upload', 'MediaController@upload');
    Route::post('media/upload/editor', 'MediaController@editorUpload');

    // Auth
    Route::get('login', ['uses' => 'HomeController@login', 'as' => 'login']);
    Route::get('register', 'HomeController@login');
    Route::get('login/{provider}', 'Auth\AuthController@redirectToProvider');
    Route::get('login/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::post('register', 'Auth\AuthController@postRegister');
    Route::get('logout', 'Auth\AuthController@logout');

    /* Package */
    Route::get('packages', 'PackageController@index');
    Route::get('packages/{slug}', 'PackageController@category');
    Route::get('package/{slug}', 'PackageController@show');
    Route::post('/package/search', 'PackageController@search');

    /* Cart */
    Route::group(['prefix' => 'cart', 'middleware' => 'auth'], function () {
        Route::get('/', 'CartController@index');
        Route::post('/', 'CartController@store');
        Route::post('/update', 'CartController@update');
        Route::post('/destroy', 'CartController@destroy');
    });

    /* Customer */
    Route::group(['prefix' => 'customer', 'middleware' => 'auth'], function () {
        Route::get('/profile', 'CustomerController@show');
        Route::get('/profile/edit', 'CustomerController@edit');
        Route::post('/profile/store', 'CustomerController@store');
        Route::post('/profile/update', 'CustomerController@update');
    });

    /* Checkout */
    Route::group(['prefix' => 'checkout', 'middleware' => 'auth.checkout'], function () {
        Route::get('/customer', 'CheckoutController@customer');
        Route::get('/payment/{id}', 'CheckoutController@payment');
        Route::get('/confirmation/{id}', 'CheckoutController@confirmation');
        Route::post('/store/{id}', 'CheckoutController@store');
        Route::post('/customer', 'CustomerController@store');
    });

    /* Blog */
    Route::get('/blog', 'BlogController@index');
    Route::get('/posts/{slug}', 'BlogController@category');
    Route::get('/post/{slug}', 'BlogController@show');

    /* Contato */
    Route::get('/contact', 'HomeController@contact');
    Route::post('/contact', 'HomeController@contact');

    Route::get('sitemap.xml', function () {
        return \App\Models\SEO::sitemap();
    });

    /* Page */
    Route::get('/404', 'PageController@notFound');
    Route::get('/{slug}', 'PageController@show');
});
