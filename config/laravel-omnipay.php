<?php

return [

	// The default gateway to use
	'default' => 'paypal',

	// Add in each gateway here
	'gateways' => [
		'creditcard' => [
			'driver' => 'PayPal_Rest',
			'options' => [
				'username' => env('PAYPAL_USERNAME'),
				'password' => env('PAYPAL_PASSWORD'),
				'signature' => env('PAYPAL_SIGNATURE'),
				'clientId' => env('PAYPAL_KEY'),
				'secret' => env('PAYPAL_SECRET'),
				'testMode' => (env('APP_ENV') == 'local' || env('APP_ENV' == 'test')),
				'solutionType'   => '',
				'landingPage'    => '',
				'headerImageUrl' => ''
			]
		],
		'paypal' => [
			'driver'  => 'PayPal_Express',
			'options' => [
				'username' => env('PAYPAL_USERNAME'),
				'password' => env('PAYPAL_PASSWORD'),
				'signature' => env('PAYPAL_SIGNATURE'),
				'testMode' => (env('APP_ENV') == 'local' || env('APP_ENV' == 'test')),
				'solutionType'   => '',
				'landingPage'    => '',
				'headerImageUrl' => ''
			]
		]
	]

];
