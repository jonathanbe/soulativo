var elixir = require('laravel-elixir'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    rjs = require('gulp-requirejs'),
    notify = require('gulp-notify'),
    minify = require('gulp-minify'),
    clean = require('gulp-clean');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    /*
     * Main tasks
     */
    elixir.config.assetsPath = 'resources/assets';
    elixir.config.publicPath = 'public';

    mix.sass('app.scss');

    new elixir.Task('requirejs', function () {
        return rjs({
            baseUrl: 'resources/assets/js',
            name: 'requireLib',
            out: 'app.js',
            include: [
                'app',
                'async',
                'bootstrap',
                'jquery',
                'underscore',
                'picker',
                'swiper',
                'stripe',
                'moip',
                'dante'
            ],
            paths: {
                'async': '../../../node_modules/requirejs-plugins/src/async',
                'bootstrap': '../../../node_modules/bootstrap/dist/js/bootstrap',
                'jquery': '../../../node_modules/jquery/dist/jquery',
                'jquery-validate': '../../../node_modules/jquery-validation/dist/jquery.validate',
                'jquery-mask': '../../../node_modules/jquery-mask-plugin/dist/jquery.mask',
                'underscore': '../../../node_modules/underscore/underscore',
                'picker': '../../../node_modules/pickadate/lib/picker',
                'picker/date': '../../../node_modules/pickadate/lib/picker.date',
                'picker/time': '../../../node_modules/pickadate/lib/picker.time',
                'swiper': '../../../node_modules/swiper/dist/js/swiper',
                'requireLib': '../../../node_modules/requirejs/require',
                'moip': '../vendor/js/moip.min',
                'sanitize': '../vendor/js/sanitize',
                'stripe': '../vendor/js/stripe',
                'dante': '../vendor/js/dante-editor'
            },
            packages: [
                'language',
                'cart',
                'search',
                'auth',
                'form',
                'address',
                'user',
                'customer',
                'password',
                'payment',
                'paginate',
                'newsletter',
                'main-menu',
                'views'
            ],
            shim: {
                app: [
                    'language',
                    'views',
                    'form',
                    'cart',
                    'search',
                    'address',
                    'user',
                    'customer',
                    'password',
                    'payment',
                    'paginate',
                    'underscore',
                    'newsletter',
                    'main-menu',
                    'auth'
                ],
                dante: [
                  'jquery',
                  'underscore',
                  'sanitize'
                ],
                bootstrap: [
                  'jquery'
                ],
                underscore: {
                    exports: '_'
                },
                jquery: {
                    exports: '$'
                },
                swiper: [
                    'jquery'
                ],
                stripe: {
                    exports: 'Stripe'
                }
            }
        })
        .pipe(gulp.dest('public/js'))
        .pipe(notify("Generated file: <%= file.relative %>"));
    })
    .watch('resources/assets/js/**/*.js');

    new elixir.Task('compress', function () {
        return gulp.src('public/build/js/*.js')
            .pipe(minify({
                ext : {
                    min : '.min.js'
                }
            }))
            .pipe(clean({force : true}))
            .pipe(gulp.dest('public/build/js'))
            .pipe(notify("Compressed file: <%= file.relative %>"));
    });

    mix.version(['css/app.css', 'js/app.js']);

});
